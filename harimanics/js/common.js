$(function() {
	var topBtn = $('#page-top');
	topBtn.hide();
	//スクロールがここで設定した値に達したらボタン表示
	$(window).scroll(function () {
		if ($(this).scrollTop() > 500) {
			topBtn.fadeIn();
		} else {
			topBtn.fadeOut();
		}
	});
	//スクロールしてトップ
	topBtn.click(function () {
		$('body,html').animate({
			scrollTop: 0
		}, 300);
		return false;
	});
	$('#navigation').slimmenu({
		resizeWidth: '9999',//横幅0～9999pxまでスマホメニューにする。
		collapserTitle: '',
		animSpeed: 'medium',
		easingEffect: null,
		indentChildren: false,
		childrenIndenter: '&nbsp;'
	});
	$(".collapse-button").click(function () {
		$(this).toggleClass("active");
	});
});
$(function(){
	// #で始まるリンクをクリックしたら実行されます
	$('a[href^="#main"]').click(function() {
		// スクロールの速度
		var speed = 400; // ミリ秒で記述
		var href= $(this).attr("href");
		var target = $(href == "#" || href == "" ? 'html' : href);
		var position = target.offset().top;
		$('body,html').animate({scrollTop:position}, speed, 'swing');
		return false;
	});
 });
$(document).ready(function () {
	hsize = 700/1920*($(window).width() < 1920 ? $(window).width() : 1920);
	$(".hero").css("height", "calc(" + hsize + "px)");
	$(".hero_bg").css("height", "calc(" + hsize + "px)");
});
$(window).resize(function () {
	hsize = 700/1920*($(window).width() < 1920 ? $(window).width() : 1920);
	$(".hero").css("height","calc(" + hsize + "px)");
	$(".hero_bg").css("height", "calc(" + hsize + "px)");
});