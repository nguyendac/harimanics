window.requestAnimFrame = (function(callback) {
  return window.requestAnimationFrame ||
  window.webkitRequestAnimationFrame ||
  window.mozRequestAnimationFrame ||
  window.oRequestAnimationFrame ||
  window.msRequestAnimationFrame ||
  function(callback){
    return window.setTimeout(callback, 1000/60);
  };
})();

window.cancelAnimFrame = (function(_id) {
  return window.cancelAnimationFrame ||
  window.cancelRequestAnimationFrame ||
  window.webkitCancelAnimationFrame ||
  window.webkitCancelRequestAnimationFrame ||
  window.mozCancelAnimationFrame ||
  window.mozCancelRequestAnimationFrame ||
  window.msCancelAnimationFrame ||
  window.msCancelRequestAnimationFrame ||
  window.oCancelAnimationFrame ||
  window.oCancelRequestAnimationFrame ||
  function(_id) { window.clearTimeout(id); };
})();
function closest(el,selector) {
  // type el -> Object
  // type select -> String
  var matchesFn;
  // find vendor prefix
  ['matches','webkitMatchesSelector','mozMatchesSelector','msMatchesSelector','oMatchesSelector'].some(function(fn) {
    if (typeof document.body[fn] == 'function') {
      matchesFn = fn;
      return true;
    }
    return false;
  })
  var parent;
  // traverse parents
  while (el) {
    parent = el.parentElement;
    if (parent && parent[matchesFn](selector)) {
      return parent;
    }
    el = parent;
  }
  return null;
}
function isPartiallyVisible(el) {
  var elementBoundary = el.getBoundingClientRect();
  var top = elementBoundary.top;
  var bottom = elementBoundary.bottom;
  var height = elementBoundary.height;
  return ((top + height >= 0) && (height + window.innerHeight >= bottom));
}
window.addEventListener('DOMContentLoaded',function(){
  if(document.getElementById('mv')){
    new Slide();
  }
  new Sticky();
  new Effect();
  if(document.getElementById('popup')){
    new Popup();
  }
  if(document.getElementById('paccess')){
    new Access();
  }
})
var Sticky = (function () {
  function Sticky() {
    var s = this;
    this._target = document.getElementById('header');
    this._for = function (top) {
      if (top > 0) {
        s._target.classList.add('fixed');
        document.body.style.paddingTop = s._target.clientHeight + 'px';
      } else {
        document.body.style.paddingTop = 0;
        s._target.classList.remove('fixed');
      }
    };
    this.handling = function () {
      var _top = document.documentElement.scrollTop || document.body.scrollTop;
      var _left = document.documentElement.scrollLeft || document.body.scrollLeft;
      s._for(_top);
    };
    window.addEventListener('scroll', s.handling, false);
    window.addEventListener('resize', s.handling, false);
    window.addEventListener('load', s.handling, false);
  }
  return Sticky;
})()
var Slide = (function(){
  function Slide(){
    var s = this;
    this.target = 'mv';
    this.step  = 0.0005;
    this.scale = 1;
    this.currentSlide = 0;
    this.opacity = 0;
    this.timer;
    this.timeout;
    this.img;
    this.eles = document.getElementById(this.target).querySelectorAll('figure img');
    this.func_transition = function(){
      s.img = s.eles[s.currentSlide];
      s.img.parentNode.style.opacity = 1;
      s.img.parentNode.classList.add('active');
      s.scale+=s.step;
      if(s.scale >= 1.15){
        s.img.parentNode.style.opacity = 0;
        if(s.scale >= 1.18) {
          s.img.parentNode.classList.remove('active');
          s.currentSlide+=1;
          s.scale = 1;
          if(s.currentSlide > s.eles.length-1) {
            s.currentSlide = 0;
          }
        }
      }
      s.timer = window.requestAnimFrame(s.func_transition);
    }
    this.sizeWindow = function(){
      Array.prototype.forEach.call(s.eles,function(el,i){
        el.parentNode.style.opacity = s.opacity;
        el.parentNode.classList.remove('active');
      });
    }
    window.addEventListener('load',function(){
      s.sizeWindow();
    })
    // this.sizeWindow();
    this.func_transition();
  }
  return Slide;
})()
var Access = (function(){
  function Access(){
    var p = this;
    this._target = document.getElementById('paccess');
    this._content =  document.getElementById('content_paccess');
    this._events = document.querySelectorAll('.call_access');
    Array.prototype.forEach.call(p._events,function(el){
      el.addEventListener('click',function(e){
        e.preventDefault();
        p._target.classList.add('open');
        document.body.style.overflow = 'hidden';
      })
    })
    this._target.querySelector('.close').addEventListener('click',function(e){
      e.preventDefault();
      p._target.classList.remove('open');
      document.body.style.overflow = 'inherit';
    })
    document.addEventListener('keydown',function(e){
      if(e.keyCode  === 27){
        p._target.querySelector('.close').click();
      }
    });
    document.addEventListener('click', function (event) {
      // If the click happened inside the the container, bail
      if (closest(event.target,'.popup_main') || event.target.classList.contains('popup_main')) {
        return;
      } else {
        if(event.target.classList.contains('popup')){
          p._target.querySelector('.close').click();
        }
      }
    }, false);
  }
  return Access;
})()
var Popup = (function(){
  function Popup(){
    var p = this;
    this._target = document.getElementById('popup');
    this._content =  document.getElementById('content_popup');
    this._events = document.querySelectorAll('.call_popup');
    Array.prototype.forEach.call(p._events,function(el){
      el.addEventListener('click',function(e){
        e.preventDefault();
        p._target.classList.add('open');
        document.body.style.overflow = 'hidden';
      })
    })
    this._target.querySelector('.close').addEventListener('click',function(e){
      e.preventDefault();
      p._target.classList.remove('open');
      document.body.style.overflow = 'inherit';
    })
    document.addEventListener('keydown',function(e){
      if(e.keyCode  === 27){
        p._target.querySelector('.close').click();
      }
    });
    document.addEventListener('click', function (event) {
      // If the click happened inside the the container, bail
      if (closest(event.target,'.popup_main') || event.target.classList.contains('popup_main')) {
        return;
      } else {
        if(event.target.classList.contains('popup')){
          p._target.querySelector('.close').click();
        }
      }
    }, false);
  }
  return Popup;
})()
var Effect = (function(){
  function Effect(){
    var e = this;
    this._els = document.querySelectorAll('.effect');
    this.handling = function(){
      Array.prototype.forEach.call(e._els,function(el){
        if(isPartiallyVisible(el)) {
          el.classList.add('active');
        }
      })
    }
    this.handling();
    window.addEventListener('scroll',e.handling,false);
  }
  return Effect;
})()