<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='TOPページ';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='website';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='top';

require_once './php/.header.php';//ヘッダー読み込み
?>
<div class="hero-container">
  <!-- <div class="hero_slide">
    <div class="hero hero1">
      <img src="img/hero1_up.png?v=3" alt="">
      <div class="hero_bg"></div>
      <div class="hero_text">この視点、初体験</div>
    </div>
    <div class="hero hero2">
      <img src="img/hero2_up.png?v=3" alt="">
      <div class="hero_bg"></div>
      <div class="hero_text">蓄積された、経験</div>
    </div>
    <div class="hero hero3">
      <img src="img/hero3_up.png?v=3" alt="">
      <div class="hero_bg"></div>
      <div class="hero_text">結果に、こだわる</div>
    </div>
  </div> -->
  <div id="mv" class="mv">
    <figure class="leftToRight">
      <img src="./img/tori_2.jpg" alt="">
      <figcaption>この視点、初体験</figcaption>
    </figure>
    <figure class="rightToLeft">
      <img src="./img/otoko_2.jpg" alt="">
      <figcaption>蓄積された、経験</figcaption>
    </figure>
    <figure class="centerToCenter">
      <img src="./img/hana_2.jpg" alt="">
      <figcaption>結果に、こだわる</figcaption>
    </figure>
  </div>
  <div class="create-together">CREATE TOGETHER</div>
  <a href="#main"><div class="btn_scrolldown"><span>SCROLL</span></div></a>
</div>
<main id="main" role="main">
  <article class="about">
    <h1 class="top_h1 animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once" data-notation="企業ビジョン">VISION</h1>
    <div class="subject">
      <h2 class="about_h2 animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once">つたわり、つながり、ひろがっていく。</h2>
      <p class="about_p animated" data-scroll="toggle(.fadeInRight, .invisible) addHeight once">報恩の連鎖で世の中は成長発展していきます。<br>その連鎖のお手伝いをすることで、ハリマニックスはお客様と共に成長してまいります。</p>
    </div>
  </article>
  <article id="works" class="works">
    <h1 class="top_h1 animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once" data-notation="制作実績">PAST PROJECTS</h1>
    <div class="subject top_works_container">
      <div class=" df fw-w jc-sb">
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/kounotorakku.jpg?v=2" alt="">
          <figcaption>
            <h2>河野トラック株式会社</h2>
          </figcaption>
          <a href="/works/kounotorakku.php"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/takasago.jpg?v=2" alt="">
          <figcaption>
            <h2> 高砂市観光交流ビューロー</h2>
          </figcaption>
          <a href="/works/takasago-tavb.php"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/setsunan.jpg?v=2" alt="">
          <figcaption>
            <h2>摂南大学</h2>
          </figcaption>
          <a href="/works/setsunan-university.php"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/epinygel.jpg?v=2" alt="">
          <figcaption>
            <h2>Epiny Gel</h2>
          </figcaption>
          <a href="/works/epinygel.php"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/okubotekkou.jpg?v=2" alt="">
          <figcaption>
            <h2>株式会社大窪鐵工所</h2>
          </figcaption>
          <a href="/works/okubotk.php"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/hiroshimakokusai.jpg?v=2" alt="">
          <figcaption>
            <h2>広島国際大学</h2>
          </figcaption>
          <a href="/works/hirokoku-u.php"></a>
        </figure>
        <!-- <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/daieirasen.jpg?v=2" alt="">
          <figcaption>
            <h2>株式会社大栄螺旋工業</h2>
          </figcaption>
          <a href="/works/daieirasen.php"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/yakitatei.jpg?v=2" alt="">
          <figcaption>
            <h2>yakitatei</h2>
          </figcaption>
          <a href="/works/"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/rohto.jpg" alt="">
          <figcaption>
            <h2>ロート製薬株式会社</h2>
          </figcaption>
          <a href="/works/rohto_shinryoku.php"></a>
        </figure> -->
<!-- 				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/hamada.jpg" alt="">
          <figcaption>
            <h2>株式会社浜田工務店</h2>
          </figcaption>
          <a href="/works/hamadakoumuten.php"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/nakachu.jpg" alt="">
          <figcaption>
            <h2>株式会社ナカチュー</h2>
          </figcaption>
          <a href="/works/nakachu.php"></a>
        </figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <img src="/works/img/hishidasangyo.jpg" alt="">
          <figcaption>
            <h2>菱田産業株式会社</h2>
          </figcaption>
          <a href="/works/hishidasangyo.php"></a>
        </figure> -->
      </div>
      <a class="btn btn_viewmore animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="works/">VIEW MORE</a>
    </div>
    <div class="works_links df fw-w jc-c">
      <a href="http://空撮館.com/" target="_blank" class="animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once"><img src="img/btn_kusatukan.png" alt=""></a>
      <a href="https://www.laminate-online.net/" target="_blank" class="animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once"><img src="img/btn_laminate.png" alt=""></a>
      <a href="http://空撮館.com/discover_hyogo/" target="_blank" class="animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once"><img src="img/btn_discover.png" alt=""></a>
    </div>
  </article>
  <article id="service" class="service">
    <h1 class="top_h1 animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once" data-notation="ビジネス">BUSINESS</h1>
    <div class="sales_promotion subject df jc-sb effect">
      <div class="service_content">
        <h2 class="top_service_h2">SALES PROMOTION<br><span>セールスプロモーション部</span></h2>
        <p>クライアントに対して最適な手段で働きかけ、需要を刺激・喚起・増大させる一連の活動をするプロ集団です。略称SP部。主に紙媒体では名刺・封筒・会社概要・製品パンフレット作成、印刷関連展示会の実施、POP広告、折込チラシ等を通じてクライアント先支援を行っています。また、WEB媒体では各種WEBサイト制作、WEBマーケティング、システム開発、動画制作等を通じてクライアント先・商品または製品・サービス等を広く世間に告知し、関心欲をそそるためのコミュニケーション活動を行います。</p>
        <a class="btn btn_viewmore animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once" href="service/#sales">VIEW MORE</a>
      </div>
      <div class="img_wrap"><a href="service/#sales"><img src="img/service_salespromotion.jpg?v=2" alt="" class="service_img"></a></div>
    </div>
    <div class="engineering_solution subject df jc-sb fd-rr effect">
      <div class="service_content">
        <h2 class="top_service_h2">ENGINEERING SOLUTION<br><span>エンジニアリングソリューション部</span></h2>
        <p>約58年の実設計サービス実績の業務設計・構築実績の融合により現場に即した機械設計プロセスの最適化を図るプロ集団です。略称ES部。主に機械設計業務及び3Dデータモデリング・2D図面化等に関する業務支援を行うとともに、ITサポートとしてMicrosoft Office ソフト各種を中心とした汎用フォーマットを使用した複雑な図面・図表・資料作成業務代行、大量の紙資料・写真等の電子データ化・スキャニング・印刷・出力・加工等を通じて業務最適化に向けたIT支援を行います。</p>
        <a class="btn btn_viewmore animated" data-scroll="toggle(.fadeInRight, .invisible) addHeight once" href="service/#engineering">VIEW MORE</a>
      </div>
      <div class="img_wrap"><a href="service/#engineering"><img src="img/service_engineeringsolution.jpg?v=2" alt="" class="service_img"></a></div>
    </div>
  </article>
  <article id="company" class="top_company">
    <h1 class="top_h1 animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once" data-notation="企業情報">COMPANY</h1>
    <div class="subject company_text">
      <p class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once">ハリマニックス株式会社は適切な情報伝達を支援することで人とモノと社会を豊かに創造することを理念とし、1961年9月に創業いたしました。現在では兵庫県高砂市・加古川市を中心とした播州地域、近畿・中国、四国地方で国内50名のクリエイターと10社以上のパートナーのみなさまとともにクライアント様との“つたわり　つながり　ひろがっていく”を実現させるため日々研鑽し進化を続けています。</p>
      <p class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once">ハリマニックス株式会社は、クライアントの可能性を模索しているプロのビジネス集団です。</p>
      <p class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once">既存の考え方・働き方にとらわれず、業界・事業を抜本から見直し新たな可能性や真に最適と言える組織運営の取り組みを発明しています。</p>
      <p class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once">私たちの活動の根源は、お困りことから理想を、理想から物事を「つたわり　つながり　ひろがっていく」を定義する営み。</p>
      <p class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once">そして「クリエイティブと言われるレベルでそれを行うこと」が未来に必要な新しい「ヒト」「モノ」「コト」をこの社会に創り出すことと信じています。</p>
      <!-- <p class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once">そして「クリエイティブと言われるレベルでそれを行うこと」が未来に必要な新しい「モノ」「コト」をこの社会に創り出すことと信じています。</p> -->
      <a class="btn btn_viewmore animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once" href="/company/">VIEW MORE</a>
    </div>
<!-- 			<div class="company_content df fd-r">
      <div class="img_wrap animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once"><img src="img/company.jpg" alt=""></div>
    </div> -->
  </article>
  <article id="news" class="top_news animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
    <h1 class="top_h1" data-notation="お知らせ">NEWS</h1>
    <div class="subject">
      <div class="top_news_container">
        <?php include './news/.news_list.php' ?>
      </div>
      <a class="btn btn_viewmore animated fadeIn pp_news call_popup" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="#" id="pp_news">VIEW MORE</a>
    </div>
  </article>
  <article id="contact" class="contact">
    <h1 class="top_h1 animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once" data-notation="お問い合わせ">CONTACT</h1>
    <div class="wrap df fdr jcc">
      <div class="form animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
        <p>メールでのお問い合わせはこちら</p>
        <p><a href="contact/" class="btn btn_contact">CONTACT</a></p>
      </div>
      <div class="tel animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
          <p>お電話でのお問い合わせ</p>
          <p class="telno">078-443-5577</p>
          <small>8:30～17:30 ※土・日・祝日を除く</small>
      </div>
    </div>
    <div class="address animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
      <p>ハリマニックス株式会社</p>
      <p>〒676-0022 兵庫県高砂市高砂町浜田町 1-7-28</p>
      <a href="#" class="call_map btn btn_access btn_access_top call_access">ACCESS</a>
    </div>
  </article>
</main>
<div id="popup" class="popup">
  <div class="popup_main">
    <div id="content_popup" class="popup_content">
      <div class="news_item">
      	<div class="news_date">2018.10.02</div>
      	<div class="news_title"><a href="news/2018.10.02-01.php">Webサイトリニューアルのお知らせ</a></div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.08.01</div>
      	<div class="news_title"><a href="news/2018.08.01-01.php">お盆休みのお知らせ</a></div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.07.23</div>
      	<div class="news_title">姉妹会社の<a href="">菱田産業株式会社がホームページを公開しました。</a></div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.05.07</div>
      	<div class="news_title"><a href="">「SNSを活用したスマホ販促セミナー」開催レポート！</a>を公開しました。</div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.03.28</div>
      	<div class="news_title"><a href="">【経営者・WEBご担当者向け】「SNSを活用したスマホ販促セミナー」を開催します！</a>を公開しました。</div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.03.06</div>
      	<div class="news_title"><a href="">スマホ販促担当向け 「効果が目に見える スマホ集客セミナー」を開催しました！</a>を公開しました。</div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.01.18</div>
      	<div class="news_title"><a href="">【姫路限定】「脱!!折込チラシ対策セミナー」を開催します！</a>を公開しました</div>
      </div>
    </div>
    <a class="close" id="close" href="#">close</a>
  </div>
</div>
<?php
$harimap='<div id="access" class="access">
  <p><img src="/img/logo.svg" alt="" /></p>
  <div id="map" style="width:100%;height:320px;"></div>
  <script>
    function initMap() {
      var uluru = {lat: 34.752842, lng: 134.800621};
      var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 14, center: uluru, disableDefaultUI: true
      });
      var contentString = \'<h2>ハリマニックス株式会社</h2><p>〒676-0022</p><p>兵庫県高砂市高砂町浜田町 1-7-28</p><p>Tel: 078-443-5577</p><a href="https://goo.gl/maps/1DKj3fTgfsJ2" target="_blank" class="gmapbtn">Google map</a>\';
      var infowindow = new google.maps.InfoWindow({
        content: contentString
      });
      var imgPath = "./icon/icon_black.svg";
      var img = { url: imgPath, scaledSize: new google.maps.Size(36,36) };
      var marker = new google.maps.Marker({ position: uluru, map: map, icon: img });
      marker.addListener("click", function() {
        infowindow.open(map, marker);
      });
      var mapStyle = [
        { "stylers": [ { "saturation": -100 } ] },
        { "featureType": "poi.business", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }
      ];
      var mapType = new google.maps.StyledMapType(mapStyle);
      map.mapTypes.set("GrayScaleMap", mapType);
      map.setMapTypeId("GrayScaleMap");
    }
  </script>
  <script async defer src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqVnXGgDZa9SCLsj3AVqccBS6QMiW09oc&callback=initMap"></script>
</div><!-- /class="access" -->';

require_once './php/.footer.php';//フッター読み込み
?>