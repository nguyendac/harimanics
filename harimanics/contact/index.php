<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='contact';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
  <article>
    <h1 data-notation="お問い合わせフォーム">CONTACT</h1>
    <div class="subject">
      <div class="contact_txt">
        <p>制作のご依頼、お問い合わせはこちらからお願いいたします。</p>
        <p>下記をよくお読みの上、これにご同意いただいた上で入力し、<span>「確認画面にすすむ」</span>ボタンを押してください。</p>
        <h2>個人情報の取り扱いについて</h2>
        <p class="doc">ご提供いただく情報は、お客様へのお問い合わせ対応業務のみに使用されます。</p>
        <p>利用目的の達成に必要な委託又は法令に基づく場合を除き、個人情報の第三者への開示・提供等は行われません。</p>
        <p class="doc">情報のご提供は任意ですがご提供いただけない場合は、お問い合わせへの効果的な対応ができな場合がありますので予めご了承ください。</p>
        <p class="doc">お問い合わせ対応後は弊社規定に基き、一定期間保管の後に破棄または消去いたします。</p>
        <p class="doc">ご提供いただいた情報に対する利用目的の確認、開示、訂正、追加又は削除、利用停止又は提供の拒否等のご要望につきましては、下記までお問い合わせください。</p>
        <div class="contact_area_b">
          <p>個人情報保護管理者&nbsp;&nbsp;菱田真由（ヒシダ マユ）</p>
          <p class="_contact"><span>TEL 079-443-5577</span><span>FAX 079-443-1086 </span><span>E-mail. privacy@harimanics.co.jp</span></p>
          <p class="note">※個人情報保護管理者のメールアドレスでは通常のお問い合わせ・採用応募は受け付けておりません。所定のお問い合わせフォーム又は採用応募フォームをご利用ください。</p>
        </div>
      </div>
      <form action method="post" id="form-contact" enctype="multipart/form-data">
        <dl class="contactform">
          <div>
            <dt class="required">会社名</dt>
            <dd><input type="text" id="company" name="company" placeholder="播磨株式会社"></dd>
          </div>
          <div>
            <dt class="any">部署名</dt>
            <dd><input type="text" placeholder="制作課"></dd>
          </div>
          <div>
            <dt class="any">貴社Webサイト</dt>
            <dd><input type="text" placeholder="https://www.harimanics.co.jp/"></dd>
          </div>
          <div>
            <dt class="required">ご担当者名</dt>
            <dd><input type="text" placeholder="播磨 太郎"></dd>
          </div>
          <div>
            <dt class="required">メールアドレス</dt>
            <dd><input type="text" placeholder="example@harimanics.co.jp"></dd>
          </div>
          <div>
            <dt class="required">電話番号</dt>
            <dd><input type="tel" id="tel" name="tel" placeholder="079-443-5577"></dd>
          </div>
          <div>
            <dt class="required">会社所在位置</dt>
            <dd><input type="email" placeholder="兵庫県高砂市高砂町浜田町1丁目7-28"></dd>
          </div>
          <div>
            <dt class="required">お問い合わせ内容</dt>
            <dd><textarea rows="5" placeholder="例) 新しく開業するのですが、名刺やパンフレット・チラシ・ホームページなど、包括的に制作する事はできますか？"></textarea>
            </dd>
          </div>
          <div>
            <dt class="required">個人情報の取り扱いについて</dt>
            <dd>
              <span class="contactaccept_check"><input name="contact_accept" type="checkbox" value="1" id="contact_accept"><label for="contact_accept">個人情報の取り扱いに同意する</label></span>
              <span class="privacypolicy">当社の<a href="/privacypolicy/" target="_blank">個人情報取り扱いについて</a>について同意される方のみ送信できます。</span>
            </dd>
          </div>
          <!-- <div>
            <dt>メールアドレス(確認用)</dt>
            <dd><input type="email" id="mail_address_confirm" name="mail_address_confirm" placeholder="example@harimanics.co.jp"></dd>
          </div> -->
          <!-- <div>
            <dt>お問い合わせの内容<br><span id="count" class="count">残文字数 1000文字</span><script>function lengthdisp(str){var wlength = str.maxLength - str.value.length;document.getElementById("count").innerHTML = "残文字数 " + wlength + "文字";return(str);}</script></dt>
            <dd class="required"><textarea id="mail_contents" name="mail_contents" rows="5" maxlength="1000" onkeyup="lengthdisp(this)" placeholder="例) 新しく開業するのですが、名刺やパンフレット・チラシ・ホームページなど、包括的に制作する事はできますか？"></textarea></dd>
          </div> -->
        </dl>
        <!-- <div class="df fd-r fw-w ai-bl">
          <span class="contactaccept_check"><input name="contact_accept" type="checkbox" value="1" id="contact_accept"><label for="contact_accept">個人情報の取り扱いに同意する</label></span>
          <span class="privacypolicy"><a href="/privacypolicy/" target="_blank">プライバシーポリシーを新しいタブで開く</a></span>
        </div> -->
        <button type="submit" class="btn btn_submit" data-form="form-contact">確認画面に進む</button>
      </form>

    </div>
  </article>
</main>
<?php
  $harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>