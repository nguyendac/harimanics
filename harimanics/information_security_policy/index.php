<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='情報セキュリティ基本方針';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='privacypolicy';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="information security policy">情報セキュリティ基本方針</h1>
		<section>
			<p>当社は、紙媒体WEB媒体において情報加工業を中核としてお客様のニーズに応えてきました。今後も、お客様にご満足いただける製品・サービスを提供するために、高度情報化社会における情報資産を事故・災害・犯罪などの脅威から守り、お客様ならびに社会の信頼に応えるべく、情報セキュリティ基本方針を定め、当社の情報セキュリティに対する取り組みの指針といたします。 </p>
			<ol>
				<li>社内体制およびISMS基本方針の整備
					<p>当社は、セキュリティの維持及び改善のために必要な管理体制を整備し、必要な情報セキュリティ対策を社内の正式な規則として定めます。</p>
				</li>
				<li>リーダーシップにおける責任および継続的改善
					<p>当社の経営者は、本方針の遵守により、当社及びお客様の情報資産が適切に管理されるよう主導します。</p>
				</li>
				<li>法令、契約上の要求事項の遵守
					<p>当社の従業員は、事業活動で利用する情報資産に関連する法令、規制、規範及びお客様との契約上のセキュリティ要求事項を遵守します。</p>
				</li>
				<li>従業員の取組み
					<p>当社の従業員は、情報セキュリティの維持及び改善のために必要とされ知識、技術を習得し、情報セキュリティへの取り組みを確かなものにします。</p>
				</li>
				<li>違反及び事故への対応
					<p>当社は、情報セキュリティに関わる法令、規制、規範及びお客様との契約に関わる違反及び情報セキュリティ事故への対応のための体制を整備し、違反及び事故の影響を低減します。</p>
				</li>
			</ol>
			<p class="sign">平成30年8月3日<br>ハリマニックス株式会社<br>代表取締役社長 菱田好美</p>
		</section>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>