<footer class="gfoot">
	<div class="footer_wrap df fw-w jc-sb ai-bl">
		<div class="footerlogo"><img src="/img/logo-ft.png" alt=""></div>
		<small>&copy; Harimanics Inc. All Rights Reserved.</small>
		<nav class="footernav">
			<ul class="lsn nav df fw-w jc-sb">
				<?php require '.menu.php' ?>
			</ul>
		</nav>
		<p id="page-top"><a href="#top">▲</a></p>
	</div>
  <div id="paccess" class="popup">
    <div class="popup_main">
      <div id="content_paccess" class="popup_content_map">
        <?php echo $harimap ?>
        <div class="place fg2">
  				<h2>本社</h2>
  				<p>〒676-0022</p>
          <p>兵庫県高砂市高砂町浜田町1丁目7-28</p>
  				<p>TEL 079-443-5577<br>FAX 079-443-1086</p>
  			</div>
      </div>
      <a class="close" id="close" href="#">close</a>
    </div>
  </div>
	
</footer>

<!--<script src="/js/delighters.js"></script>-->

<script src="/js/ScrollTrigger.min.js"></script>
<script>
	document.addEventListener('DOMContentLoaded', function() {
		var trigger = new ScrollTrigger();
	});
</script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="/js/jquery-3.3.1.min.js"><\/script>')</script>
<script src="/js/jquery.slimmenu.min.js"></script>
<script src="/js/common.js"></script>
<script src="/js/script.js"></script>
<script>
</script>
</body>
</html>