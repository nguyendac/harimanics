<?php
/*使ってる変数一覧*/
/*
$kw='';//metaのキーワード
$d='';//metaのdescription
$title='';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでサブディレクトリ/ファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='';//<body>にclassを付与する用
*/
?>
<!DOCTYPE html>
<html lang="ja" dir="ltr">
<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# <?php echo $og_type ?>: http://ogp.me/ns/article#">
<meta charset="UTF-8">
<!--[if IE]><meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1"><![endif]-->
<meta name="viewport" content="width=device-width, initial-scale=1, viewport-fit=cover">
<meta name="keywords" content="<?php echo $kw ?>">
<meta name="description" content="<?php echo $d ?>">
<title>ハリマニックス株式会社 | <?php echo $title ?></title>
<?php //OGP設定ここから ?>
<meta property="og:locale" content="ja_JP">
<meta property="og:site_name" content="ハリマニックス株式会社">
<meta property="og:title" content="<?php echo $og_title ?>"><?//ページのタイトル?>
<meta property="og:type" content="<?php echo $og_type ?>"><?//ページの種類?>
<meta property="og:url" content="http://.co.jp/<?php echo $og_url ?>"><?//ページのURL?>
<meta property="og:image" content="http://.co.jp/img/<?php echo $og_img ?>"><?//サムネイル画像のURL?>
<meta property="og:description" content="<?php echo $og_description ?>"><?//ページのディスクリプション?>
<meta property="fb:app_id" content="xxxxxxxxxxxxxxxx">
<?php //※Twitter共通設定 ?>
<meta name="twitter:card" content="summary_large_image<?//Twitterカードの種類?>">
<meta name="twitter:title" content="">
<meta name="twitter:description" content="">
<meta name="twitter:image" content="">
<?php //OGP設定ここまで ?>
<?php echo $canonical ?>
<?php echo $other01 ?>
<link rel="stylesheet" href="/css/slimmenu.min.css">
<link rel="stylesheet" href="/css/animate.min.css">
<link rel="stylesheet" href="/css/style.css?d=<?php echo date('Y/m/d-H:i:s'); ?>">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
<link rel="stylesheet" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<?php require '.icons.php' ?>
<meta name="theme-color" content="#e25449">
<?php echo $other02 ?>
<!--[if lte IE 9]>
<script src="//cdnjs.cloudflare.com/ajax/libs/html5shiv/3.7.3/html5shiv.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/respond.js/1.4.2/respond.min.js"></script>
<script src="js/flexibility.js"></script>
<script>
$(function(){
	flexibility(document.documentElement);
});
</script>
<![endif]-->
</head>
<body class="<?php echo $bodyclass ?>">
<header class="ghead df fdr jcsb" id="header">
	<div class="logo"><a href="/"><img src="/img/logo.svg?d=0417" alt="ハリマニックス株式会社" class="logoimg"></a></div>
	<nav class="gnav">
		<ul id="navigation" class="slimmenu lsn nav">
			<?php require '.menu_header.php' ?>
		</ul>
	</nav>
</header>