<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='お知らせ';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='news';

require_once '../php/.header.php';//ヘッダー読み込み
?>
<main role="main">
	<article>
		<h1 id="sales" data-notation="お知らせ">News</h1>
		<div class="news_container subject">
				<?php include '.news_list.php' ?>
        <a class="btn btn_viewmore animated fadeIn pp_news call_popup" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="#" id="pp_news">VIEW MORE</a>
		</div>
	</article>
</main>
<div id="popup" class="popup">
  <div class="popup_main">
    <div id="content_popup" class="popup_content">
      <div class="news_item">
      	<div class="news_date">2018.10.02</div>
      	<div class="news_title"><a href="news/2018.10.02-01.php">Webサイトリニューアルのお知らせ</a></div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.08.01</div>
      	<div class="news_title"><a href="news/2018.08.01-01.php">お盆休みのお知らせ</a></div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.07.23</div>
      	<div class="news_title">姉妹会社の<a href="">菱田産業株式会社がホームページを公開しました。</a></div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.05.07</div>
      	<div class="news_title"><a href="">「SNSを活用したスマホ販促セミナー」開催レポート！</a>を公開しました。</div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.03.28</div>
      	<div class="news_title"><a href="">【経営者・WEBご担当者向け】「SNSを活用したスマホ販促セミナー」を開催します！</a>を公開しました。</div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.03.06</div>
      	<div class="news_title"><a href="">スマホ販促担当向け 「効果が目に見える スマホ集客セミナー」を開催しました！</a>を公開しました。</div>
      </div>
      <div class="news_item">
      	<div class="news_date">2018.01.18</div>
      	<div class="news_title"><a href="">【姫路限定】「脱!!折込チラシ対策セミナー」を開催します！</a>を公開しました</div>
      </div>
    </div>
    <a class="close" id="close" href="#">close</a>
  </div>
</div>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>