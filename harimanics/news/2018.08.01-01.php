<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='news';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="お知らせ">NEWS</h1>
		<div class="subject">
			<h2>お盆休みのお知らせ</h2>
			<div class="date">2018.08.01</div>
			<p>平素は格別のお引き立てを賜わり、誠に有難うございます。</p>
			<p>弊社のお盆休業についてお知らせ致します。</p>
			<p>誠に勝手ながら弊社のお盆休みは下記の通り休業とさせて頂きます。</p>
			<p>8月11日(金)～16日(木)の6日間</p>
			<p>期間中のお問い合わせに関しては弊社のホームページの「お問い合わせフォーム」もしくはメールにてお願い致します。</p>
			<p>大変ご不便おかけいたしますが、何卒ご了承くださいますよう宜しくお願い申し上げます。</p>
		</div>
		<a href="/" class="btn btn_gohome">HOMEへ戻る</a>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>