<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='採用情報';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='recruit';

require_once '../php/.header.php';//ヘッダー読み込み
?>
<main role="main">
	<article>
		<h1 id="sales" data-notation="採用情報">RECRUIT</h1>
		<div class="recruit_container subject">
			<p>弊社では随時やる気・熱意のある人材を求めています。</p>
			<p>自分の能力を発揮して弊社を活躍の場にしていただける方のご応募をお待ちしております。</p>
			<div class="df fw-w jc-sb">
				<a href="recruit.php#clerk" class="btn btn_recruit">事務スタッフ（設計補助）</a>
				<a href="recruit.php#ne" class="btn btn_recruit">ネットワークエンジニア</a>
				<a href="recruit.php#operator" class="btn btn_recruit">パソコンオペレーター</a>
				<a href="recruit.php#engineer" class="btn btn_recruit">WEBエンジニア</a>
			</div>
		</div>
	</article>
</main>

<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>