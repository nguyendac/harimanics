<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service salespromotion';

require_once '../php/.header.php';//ヘッダー読み込み
include_once 'img/webmarketing_icon.svg'; //@232*150
?>

<main role="main">
	<article>
		<h1 data-notation="WEBマーケティング">WEB MARKETING</h1>
		<div class="subject">
		<h2>戦略的ウェブマーケティング</h2>
		<p>Webマーケティングと言っても、その全体像を具体的にイメージできる人は少ないのではないでしょうか。本気で成果をあげたいと思ったら、まずはWebマーケティング全体を俯瞰した上で、戦略的に施策を組み立てていくことが重要だとハリマニックスは考えます。</p>
		<section>
			<h3 class="diamond">Webマーケティングの特徴</h3>
			<p>マーケティングの中でも「Webマーケティング」には以下のような特徴があります。</p>
			<section class="numbering">
				<h4>効果測定性</h4>
				<p>Webマーケティングでは、施策の効果や費用などをすべて数値化することが可能であり、それによって施策の検証をすることができます。</p>
			</section>
			<section class="numbering">
				<h4>ハイスピード</h4>
				<p>Webマーケティングには、施策の意思決定から実行までにほとんど時間がかかりません。それにより、従来のマーケティング手法とはスピード感が違います。</p>
			</section>
			<section class="numbering">
				<h4>ローコスト</h4>
				<p>Webマーケティングは、極小規模から始めることが可能なため、予算の少ない企業でも、効果のでやすい施策から始めることができます。</p>
			</section>
			<section class="numbering">
				<h4>パーソナルアプローチ</h4>
				<p>Webマーケティングでは、ターゲット一人ひとりを対象として施策を行うことができます。それにより無駄が少ないため、従来の手法よりもコスト効率がよいと言えます。</p>
			</section>
		</section>
		<hr>
		<section class="counterreset">
			<h3 class="diamond">Webマーケティングの全体像</h3>
			<div class="policy">
				<p>Webマーケティングの手法は、大きく「集客」「接客」「再来訪」の３つの施策に分けられます。</p>
				<ul>
					<li>集客：Webサイトにユーザを集めるための施策。</li>
					<li>接客：Webサイト上で、ユーザに特定のアクション（購買・資料請求・問い合わせなど）をしてもらうための施策。加えて、そうしたアクションを起こさせるためにユーザを知るための施策。</li>
					<li> 再来訪：一度Webサイトへ来たユーザに、もう一度来てもらったり、ユーザとの関係を保ったりするための施策。</li>
				</ul>
			</div>
			<div class="img_wrap"><img src="img/web_pro_chart.svgz" class="b1"></div>
			<section class="numbering">
				<h4>集客</h4>
				<dl class="df fd-r fw-w jc-sb">
					<div class="method">
						<dt>
							<div class="method_title l1">①SEO（検索エンジン最適化）</div>
							<svg class="wm"><use xlink:href="#wm_search" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★★★☆</span></li>
								<li>即効性　<span class="star">★☆☆☆☆</span></li>
							</ul>
							<p>GoogleやYahoo!の検索結果からアクセス増加を狙うための施策です。基本的に即効性はありませんが、中長期的にユーザの自然流入の増加が見込めるため、ユーザ獲得には必須の施策と言えます。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l1">②リスティング広告</div>
							<svg class="wm"><use xlink:href="#wm_listing" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★☆☆☆</span></li>
								<li>即効性　<span class="star">★★★☆☆</span></li>
							</ul>
							<p>検索結果ページに表示する広告のことです。「Google AdWords」や「Yahoo!プロモーション広告」、「REMORA Listing」などの広告元に料金を払えばすぐに上位に表示することが可能なため、SEOより即効性の高い施策とは言えますが、細かな運用や管理が必要です。SEOとリスティング広告をあわせて、SEMと呼びます。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l1">③アドネットワーク広告</div>
							<svg class="wm"><use xlink:href="#wm_adnetwork" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★☆☆☆</span></li>
								<li>即効性　<span class="star">★★☆☆☆</span></li>
							</ul>
							<p>Web上のさまざまなメディアに、バナー広告やテキスト広告を配信できるネットワークのことです。リスティング広告よりもはるかに多くのユーザにリーチすることが可能です。有名なものに「Google Adsense」や「Yahoo!ディスプレイアドネットワーク」などがあります。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l1">④アフィリエイト広告</div>
							<svg class="wm"><use xlink:href="#wm_affiliate" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★☆☆☆</span></li>
								<li>即効性　<span class="star">★★☆☆☆</span></li>
							</ul>
							<p>ブログやWebサイト、メールマガジンなどにリンクを貼ってもらい、そのリンクを経由して「申込」「購入」「問い合わせ」「資料請求」などの成果が発生した場合に、そのリンクを貼った媒体（アフィリエイター）に広告主が報酬を支払う成果報酬型広告のことをいいます。成果報酬のためコスト効率はいいですが、ユーザ獲得母数が比較的少ないので、ブランドイメージが傷つく可能性があるなどのリスクがあります。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l2">⑤SMM<br><small>（ソーシャルメディアマーケティング）</small></div>
							<svg class="wm"><use xlink:href="#wm_smm" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★☆☆☆☆</span></li>
								<li>即効性　<span class="star">★☆☆☆☆</span></li>
							</ul>
							<p>FacebookやTwitterなどのSNSを自社で運用することで、ユーザにアピールして集客をはかる施策です。話題性がある投稿は、ユーザ同士の口コミにより勝手に拡散されることも期待できます。ただし、継続的にユーザと直接関わりも持つ必要があり、同時にブランドマネジメントも必要だと言えます。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l1">⑥SNS広告</div>
							<svg class="wm"><use xlink:href="#wm_sns" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★☆☆☆</span></li>
								<li>即効性　<span class="star">★★★☆☆</span></li>
							</ul>
							<p>FacebookやTwitter、LINEなどのSNSメディアに配信する広告です。<br>リスティング広告と同じようにSNSのページ内にすぐに広告を配信することができます。こちらも効果を出すためには、細かな運用管理が必要です。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l2">⑦他メディアへの広告出稿<br><small>（純広告、ネイティブ広告など）</small></div>
							<svg class="wm"><use xlink:href="#wm_othermedia" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★★★☆</span></li>
								<li>即効性　<span class="star">★★★☆☆</span></li>
							</ul>
							<p>既にアクセスが多くあるサイトやアプリに広告を掲載することで、そこからアクセス流入を狙う施策です。掲載できるメディアには、いくらで、どんな人に、どのくらいリーチできるかを示した「媒体資料」を用意している場合が多いので、それらを参照すると具体的なイメージがつくでしょう。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l2">⑧オフライン施策<br><small>（街頭広告、テレビCM、新聞雑誌など）</small></div>
							<svg class="wm"><use xlink:href="#wm_offline" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★★★★</span></li>
								<li>即効性　<span class="star">★★★★☆</span></li>
							</ul>
							<p>Webサイトへの誘導施策は、Web上だけにとどまりません。コストはかかりますが、施策によって爆発的にユーザを誘導できることがあります。</p>
						</dd>
					</div>
					<div class="method m0"></div>
				</dl>
			</section>
			<section class="numbering">
				<h4>接客</h4>
				<dl class="df fd-r fw-w jc-sb">
					<div class="method">
						<dt>
							<div class="method_title l2">①LPO<br><small>（ランディングページ最適化）</small></div>
							<svg class="wm"><use xlink:href="#wm_lpo" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★☆☆☆</span></li>
								<li>即効性　<span class="star">★★★☆☆</span></li>
							</ul>
							<p>サイトの入口となるページを最適化するための施策です。インターネット広告からの流入にあわせて専用のページを作ったり、複数の画面を用意してどの画面がよいかをテストするABテストを実施したりする場合もあります。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l1">②Web接客ツール</div>
							<svg class="wm"><use xlink:href="#wm_service" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★★☆☆</span></li>
								<li>即効性　<span class="star">★★★☆☆</span></li>
							</ul>
							<p>サイトに来ているユーザに、リアルタイムでチャットを送ったり、クーポンを発行したりできるツールです。まだ導入しているWebサイトは少ないですが、注目されているツールです。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title l1">③EFO</div>
							<svg class="wm"><use xlink:href="#wm_efo" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★☆☆☆</span></li>
								<li>即効性　<span class="star">★★★☆☆</span></li>
							</ul>
							<p>Webフォームを最適化するための施策です。せっかく十分にユーザの興味を惹く（ユーザを満足させる）ことができても、フォームページで離脱を招いていることがあります。「申込」「購入」「問い合わせ」「資料請求」などの成果地点に近いページでの施策になるため、母数さえあれば比較的即効性があります。</p>
						</dd>
					</div>
					<div class="method m0"></div>
				</dl>
			</section>
			<section class="numbering">
				<h4>再来訪</h4>
				<dl class="df fd-r fw-w jc-sb">
					<div class="method">
						<dt>
							<div class="method_title">①リターゲティング広告</div>
							<svg class="wm"><use xlink:href="#wm_retargeting" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★☆☆☆</span></li>
								<li>即効性　<span class="star">★★★★☆</span></li>
							</ul>
							<p>一度サイトへ訪問したことのあるユーザに対して広告を配信して、再来訪を促す施策です。サイトへ来たことのあるユーザはサイトへの興味関心度が高いため、コスト効率がよいです。ただ、アクセスが極端に少ないサイトでは行うことができません。配信対象者を増やすために、集客施策と同時に行う必要があります。</p>
						</dd>
					</div>
					<div class="method">
						<dt>
							<div class="method_title">②メールマーケティング</div>
							<svg class="wm"><use xlink:href="#wm_mail" /></svg>
						</dt>
						<dd>
							<ul class="marks">
								<li>コスト　<span class="star">★★☆☆☆</span></li>
								<li>即効性　<span class="star">★★★☆☆</span></li>
							</ul>
							<p>ユーザにメールを配信することで、来訪を促す施策です。単に全員に同じメールを配信するだけでなく、見込みの高い特定の人にだけメールを送ったり、クーポンを配信したりすることによって、再来訪を狙うこともあります。</p>
						</dd>
					</div>
					<div class="method m0"></div>
					<div class="method m0"></div>
				</dl>
			</section>
		</section>
		<hr>
		<section>
			<p>Webマーケティングで重要なことは、これらの施策をすべて行うことではなく、すべての施策の特徴を押さえた上で、自社のターゲットが最も多く集まるところに優先順位をつけて予算を投下することです。</p>
			<p>ハリマニックスは、クライアントのコトを正しく理解し、クライアントが抱える課題とニーズを探り、製品やサービス、市場分析を行うことでクライアントの他社との違いや強みをしっかりと伝えること、顧客へ共感や信頼、感動を与え、自然にモノが売れる流れを作り、クライアントと顧客の輪を次々と広げ、広がり繋がっていけるWebマーケティングをご提案いたします。</p>
		</section>
    <div class="btn_service df jc-c">
      <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
    </div>
	</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>