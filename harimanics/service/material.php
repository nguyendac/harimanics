<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service enginnering';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="資料作成支援">MATERIAL</h1>
		<section class="subject">
			<p>画像、写真編集、PDFデータ作成・編集、図面着色、図表トレース、翻訳、関数・マクロ(管理台帳、スケジュール表、勤怠管理、売上管理、値抽出等)、VISIO、テープ起こし、OCR、プレゼン資料作成</p>
			<p>Word・Excel・PowerPoint・Illustrator・Photoshop等での作成</p>
		</section>
    <div class="btn_service df jc-c">
      <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
    </div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>