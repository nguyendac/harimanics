<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service salespromotion';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="WEBシステム開発">WEB SYSTEM DEVELOPMENT</h1>
		<section class="subject">
			<p>業務のお困りごとはありませんか？問題解決・改善・効率化から新サービス提供まで、WEBを使ったシステム開発を行います。</p>
			<p>お客様のお声をじっくりとヒアリングさせて頂いて、最適なご提案を行います。</p>
			<p>オンプレシステムのオープン化に関しましても、ご相談ください。社内ネットワークに依存したシステムから、クラウドへと再構築のご提案もさせて頂きます。</p>
			<h2 class="diamond">対応可能言語</h2>
			<p>HTML5 / CSS3 / JavaScript(ES5, 6) / TypeScript / PHP / MySQL / Python / 他</p>
		</section>
	</article>
	<article>
		<h1 data-notation="画像データ管理運用">DATA MANAGEMENT</h1>
		<section class="subject">
			<p>写真・画像をはじめとするデータの共有・管理専用クラウドサービス「サクラク」のご提供です。</p>
			<p>複数に渡る部署、事業所といった場所を問わず、データを簡単に共有管理することが出来ます。</p>
			<p>AWSを採用することによって、動きはサクサク、またフォルダ管理以外にも検索機能やタグ付け機能があるので、管理もラクラク！</p>
			<p>さらにAIによる画像解析の採用により、検索機能の強化も。ますます便利になります！</p>
		</section>
	</article>
	<article>
		<h1 data-notation="AWS導入支援">AWS SUPPORT</h1>
		<section class="subject">
			<p>Amazon提供のクラウドプラットフォームAWS導入の予定はございますか？</p>
			<p>音声認識・AI・IoTといった、今までにない新しいソリューションについてもご相談下さい！</p>
		</section>
	</article>
	<div class="subject works_links">
		<a href="http://www.sakuraku.jp/" target="_blank" ><img src="img/btn_sakuraku.png" alt=""></a>
	</div>
  <div class="btn_service df jc-c">
    <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
  </div>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>