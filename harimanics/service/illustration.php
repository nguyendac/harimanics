<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service salespromotion';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="イラストレーション制作">GRAPHIC ILLUSTRATION</h1>
		<section class="subject">
			<p>雑誌や広告、チラシ、パンフレット、ノベルティなど、様々な紙媒体のコミュニケーションツールを制作しております。</p>
			<p>また企業のブランドイメージの再生や採用にコミットするための企画提案からデザインおよび制作までいたします。</p>
			<p>グラフィックデザインが「ビジュアルコミュニケーション」の1つと言われるように、ハリマニックスのデザインを通して人と人を繋ぎ、お客様の未来とモノづくりのお手伝いをします。</p>
      <div class="btn_service df jc-c">
        <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
      </div>
		</section>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>