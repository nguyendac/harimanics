<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service salespromotion';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="動画制作">MOVIE PRODUCTION</h1>
		<section class="subject">
			<p>企業・商品ブランド動画や社内コミュニケーションムービー、Webサイトやパンフレットだけでは伝わらないリアルを、音と映像で伝えることができるのが動画／映像の特長です。</p>
			<p>ものづくり企業であれば、一つひとつ丁寧に磨き上げるものづくりの姿勢や考え方を、サービスを商品とする企業であれば、密着取材をもとに構成するドキュメンタリーによって、ダイレクトにメッセージを訴求するなど、その表現方法は多種多様です。</p>
			<p>さまざまな観点から企画検討を行う事で、高品質な動画・映像に仕上げていきます。</p>
			<div class="btn_service df jc-c">
		        <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
		      </div>
		</section>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>