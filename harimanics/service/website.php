<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service salespromotion';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="WEBサイト制作">SITE CREATION</h1>
		<section>
			<h2>ウェブサイト制作フロー</h2>
			<p>デザインやウェブサイト制作のディレクション経験がない場合、自社のウェブサイトをどのようにして制作すればいいのか戸惑う方も多いのではないでしょうか。</p>
			<p>当社のウェブサイト制作の一連の流れは以下のとおりとなります。</p>
			<p>なるべく難しい言葉を使わず、クライアントとなる皆さまが理解しやすいよう平易な言葉で書いております。</p>
			<p><small>※ウェブサイト制作の概要はつかめるかと思いますが、クライアント様ごとにウェブサイト制作意義や目的に合わせ内容は異なりますので一般的なウェブサイト制作時のフローをまとめたものとしてご参照いただければ幸いです。</small></p>
			<section class="numbering">
				<h3>ターゲットや ペルソナ、ゴールを設定する</h3>
				<p>クライアント様の中には、老若男女問わず、様々な人（企業）に見てもらいたいという方もいます。万人受けするウェブサイトのほうがいいと考えるのは、ごく自然なことです。しかし、ターゲット設定をすると、そこにヒットするユーザがいた場合、訴求力が強まり、集客効果や広告効果にもプラスに影響します。</p>
				<p>ターゲットでは対象となるユーザの年齢層や職種などを設定しますが、そこからさらに踏み込んで「ペルソナ」を設定することもあります。ペルソナは、あたかも実在している人物像を描き出します。 ペルソナを設定することで、ユーザ目線でのWeb制作が可能になります。</p>
				<p>そして、ウェブサイト制作にあたっては、そこには必ずゴールが存在します。ウェブサイトをなぜ作るのか、という目的です。商品PRをしたい、販売促進をしたい、集客を加速させたい、などがそうです。ゴールが設定されていないと、ユーザはもちろん、ウェブサイト制作にかかわるデザイナーやコーダーも、一体どこを目指しているのかが分からないまま制作することになり、せっかくお金をかけてサイト制作しても望んでいた結果は到底得られるものではありません。</p>
				<p>そこで、まず当社とクライアント様で最初に行うことは、制作するウェブサイトの"ターゲットとゴールの設定"です。ここが決まれば、デザインの方向性もしっかりと定まります。</p>
			</section>
			<section class="numbering">
				<h3>ウェブサイトコンセプトを決める</h3>
				<section>
					<h4>サイトコンセプトは、以下の項目から考えると決めやすいです。</h4>
					<ol class="list_parentheses">
						<li>新規ウェブサイト制作またはリニューアルするきっかけは？</li>
						<li>リニューアルの場合、既存ウェブサイトの問題点やお困りごとは？</li>
						<li>ウェブサイトをどんな人（企業）に向けてつくるのか？　どんな人（企業）にどこをみてほしいのか？</li>
						<li>どんな情報をいつごろまでに掲載したいか？</li>
						<li>ウェブサイトでなければならない理由は？　なぜ他の媒体（チラシやDM配布など）ではいけないのか？</li>
					</ol>
				</section>
				<section>
					<h4>例えば製造業のサイトリニューアルのコンセプトを考えたとき、それぞれの回答が以下になったとします。</h4>
					<ol class="list_parentheses">
						<li>素人にわかりやすく、玄人も納得していただけ自社でしか出来ない仕事を紹介したい。</li>
						<li>情報が整理されておらず、プライベートブランドのため「素人」にわかりにくい製品が多く何をしている企業かがぼんやりとしている。既存サイトのデザインが古く魅力的に見えない。地元の採用者を増やしたい。</li>
						<li>製品に対してしっかりとしたブランディング、自社の技術・対応力をPRしていきたい。20代、30代の若者で、地元の人。信頼感・安心感を与え、信頼できる企業を見つけたいと思っている人。お問合せの向上、企業としての認知促進図りたい</li>
						<li>自社の強みPR、会社情報、代表者の想い、会社概要、沿革、企業理念、アクセス（会社所在地）、各種製品紹介、お知らせ、ブログ、リクルート（採用情報）、スタッフ紹介（先輩従業員の声）、募集要項、お問合せ</li>
						<li>Webではサービスや製品の内容をDMなどより詳しく、わかりやすく伝えられるから求人を出しても求職者は応募やお問合せをする前にスマートフォンで企業検索する若者が多く求人情報誌に記載しきれない採用情報をしっかりと盛り込めるから</li>
					</ol>
				</section>
				<p>以上から、コンセプトを考察します。非常にざっくりとですが、こんな感じです。</p>
				<p>「素人の若者でもわかりやすく、玄人も納得できる製品紹介と自社でしか出来ない仕事のPRをはかり企業のブランド価値を高め認知促進を図るサイト」</p>
			</section>
			<section class="numbering">
				<h3>競合他社からクライアントの強みや製品を知ってもらうためのポイントを整理する</h3>
				<p>ポイント（強み）は、他社と差別化し、選んでもらうための理由となります。</p>
				<p>引き続き製造業を例に考えてみます。</p>
				<ul>
					<li>パーツ調達から製品設計・成形・組み立てに至るまで、自社で一貫生産</li>
					<li>製品の設計自由度が高く、ユーザの多様なニーズに柔軟に対応できる</li>
					<li>経験豊富な職人たちが多数在籍、技術の継承と若手育成にも力を入れている</li>
				</ul>
				<p>などなど</p>
				<p>ポイント(強み)は、いくつか挙げられると思うのですが、もし1つだけ挙げるとしたら何かを考えます。</p>
				<p>これは次の、「そのサイトで一番伝えたい内容を考える」と関わります。</p>
			</section>
			<section class="numbering">
				<h3>そのサイトで一番伝えたい内容を考える</h3>
				<p>大まかなコンセプトが決定したら、次にユーザに一番伝えたい内容を考えます。</p>
				<ul>
					<li>一番のポイント(強み)となるもの</li>
					<li>ユーザが自社に製品をつくってほしいと依頼されるきっかけとなる情報</li>
					<li>競合他社と差別化できるサービスや製品</li>
				</ul>
				<p>伝えたいことが決まれば、それをキャッチーな言葉で表現するのか、魅力的なイメージとして表現するのか、どうすれば一番ユーザに伝わるのかを考えます。</p>
			</section>
			<section class="numbering">
				<h3>上記以外でユーザが必要とする情報や、伝えたい情報を再度確認しまとめる</h3>
				<p>ユーザにとって必要と思われる情報や、伝えたい情報をまとめます。</p>
				<p>1の、「どんな情報を掲載する？」でまとめた内容に、漏れがないか、もしくは不要なものがないかを再度チェックします。</p>
				<p>ここまできて、いよいよ「ウェブサイトの全体像」に取り掛かることになります。その上で、情報を紐付けたり、情報の階層化を行います。ここで簡単なサイトマップを作成します。</p>
			</section>
			<section class="numbering">
				<h3>サイト更新は誰がするか（制作ポリシー）を決める</h3>
				<p>制作ポリシーとは、「このような方針でウェブサイトを作っていきます」というルールのようなものです。例えば、「デスクトップ用とスマートフォン用でウェブサイトをわけるか、レスポンシブ対応デザインにするか」「対応ブラウザをどこまでに設定するか」などです。</p>
				<p>例えばユーザが企業様のみの場合はPCで見ていただくことがメインとなり、リクルート目的や一般ユーザもターゲットとするならスマートフォンで見やすいレスポンシブ対応は必須となります。昨今は特殊なケースを除き、スマートフォンやタブレット端末利用者を想定したレスポンシブ対応がスタンダードです。</p>
				<p>PC用のみの一般的なウェブサイトで作成するか、スマートフォン利用を想定して作成するのかを決めます。</p>
				<p>目的とターゲットによって利用頻度の高いデバイスが異なってきますので、どのデバイスをメインとして利用するウェブサイトなのかを考えます。</p>
				<p>デバイスの例は以下です。</p>
				<ul>
					<li>PC（Windows） / MAC</li>
					<li>スマートフォン（iPhone、Androidなど）</li>
					<li>タブレット（iPadなど）</li>
				</ul>
				<p>また、ウェブサイト上に採用情報ページなど求職者の個人情報を取得する場合には、プライバシーポリシー（個人情報保護方針）や利用規約など、防御的情報を準備しておきます。</p>
				<p>初めに上記のようなこの制作ポリシーを決めておかないと、デザイナーやコーダーが、それぞれ独断でデザインやコーディングを行うことになり、のちのシステム変更やメンテナンスに手間がかかってしまいます。</p>
				<p>そのため、全体的な制作ポリシーだけでなく、デザインポリシーやコーディングポリシーもここでクライアント様と当社で一緒に決めておきます。</p>
				<p>あわせて、このウェブサイトをクライアント様が更新するのか、弊社にお任せいただけるのか。例えば、新規でウェブサイトを立ち上げる場合、当社にて独自ドメイン取得代行からレンタルサーバ契約まで一括してお任せいただけますとクライアント様に手間をかけることなく、トータルコストも低くすみ、何よりデータが当社で一元管理できるのでその後の更新がスムーズです。</p>
				<p>ウェブサイトの更新は、しやすさなどの管理部分の設計に関わってきます。また、費用もそれによって異なってきます。</p>
				<p>弊社が更新する場合は、どのくらいのボリュームが、どのくらいの頻度で、いくらの金額になるのかもここでクライアント様と事前に決めていきます。</p>
			</section>
			<section class="numbering">
				<h3>サイトの周知・宣伝方法を考える</h3>
				<p>サイトが公開されたあと、どのようにしてユーザにサイトへ訪れてもらうのかを考えます。</p>
				<p>《例えば》</p>
				<ul>
					<li>検索サイトから検索してもらう</li>
					<li>TwitterやFacebookのアカウントを開設し、お店や商品の情報を発信する</li>
					<li>DMやショップカードなどの販促物にURLを記載する</li>
					<li>バナー広告やリスティング広告を出す</li>
				</ul>
				<p>などなど。</p>
				<p>これはクライアント様のご予算と関係してきますので、HTMLなどの基本的なSEO対策はして、あとはご予算とご要望に応じて当社にて別途ご提案という形になります。</p>
			</section>
			<section class="numbering">
				<h3>ページの設計図（ワイヤーフレーム）をつくる</h3>
				<p>サイトマップをもとに、各ページの設計図（ワイヤーフレーム）を描きます。</p>
				<p>一般的な企業サイトの場合、以下のような形をとることが多いです。（トップページの場合）</p>
				<p>あわせて各パーツの解説もつけてみました。</p>
				<div class="img_wrap"><img src="img/wireframe.svgz" alt="" class="m0a"></div>
			</section>
			<section class="numbering">
				<h3>サイト制作にかかわる必要な素材を書き出す</h3>
				<p>必要となる原稿や写真などを書きだします。</p>
				<p>当社にて原稿作成や写真撮影等を依頼する場合は、その分撮影料や、ライティング料などが別途かかります。</p>
				<p>また、既存パンフレットなどの紙媒体からデータを流用いただきたい場合は、あらかじめデータ手配していただき当社に支給いただくことが必要です。</p>
			</section>
			<section class="numbering">
				<h3>デザイン作成</h3>
				<p>ページの設計図をもとにデザインをします。</p>
				<p>トップページを作成後、一度提出をしてクライアント様に確認していただきます（デザインのテイストや文言のニュアンスやトーン＆マナーなど）。</p>
				<p>OKをもらったあと、他ページのデザインを展開していきます。</p>
			</section>
			<section class="numbering">
				<h3>コーディング、システム実装</h3>
				<p>デザイン確定後に、HTMLにデザインを反映します。</p>
				<p>対応デバイスやブラウザに沿ったコーディングを行います。また、この時、以下のことをあわせて考えます。</p>
				<ul>
					<li>文書の仕様をどうするか（HTML5やCSS3を使うかなど）</li>
					<li>フォルダ階層のルール</li>
					<li>ファイルの名づけルール</li>
				</ul>
				<p>また、システムが必要な場合は、コーディング後にシステムを組み込みます。</p>
				<p>システムはサイト管理やお問い合わせなどで多く使われますが、どのように動くか、あらかじめクライアント様とページの遷移内容を確認しておくことが必要です。</p>
			</section>
			<section class="numbering">
				<h3>公開・納品</h3>
				<p>公開は、当社が行う場合と、クライアント様にデータを渡してクライアント様側で公開していただく場合の2つがあります。</p>
				<p>また、後者の場合の納品物は、HTMLファイル一式であることが多いです。</p>
				<p>しかし以下もあわせて確認しておくことが必要です。</p>
				<ul>
					<li>デザインデータも納品物とするか</li>
					<li>更新マニュアルは必要か</li>
					<li>コーディングガイドライン・デザインガイドラインは必要か</li>
				</ul>
			</section>
			<section class="numbering">
				<h3>サイト最適化やSEO対策、SNSやリスティング、プレスリリースで発信する</h3>
				<p>ウェブサイトを公開した段階では、検索流入が頼みの綱となります。一方で、twitterやfacebook、instagramなどのSNSを活用すれば、こちらから戦略的に存在を認知させることができます。</p>
				<p>また、ある程度まとまった予算があれば、リスティングを打ったりプレスリリースを出すことで、自社のウェブサイトを宣伝することもできます。</p>
				<p>SEOだけに頼ってしまうと、 Googleの検索ポリシーが急に変更されて 検索結果が落ちた時に、アクセスが急激に落ちてしまうことがあります。できるだけ入り口となる媒体を分散して多く持っておくことで、そうしたリスクを回避することもできます。検索エンジン対策に、Googleのウェブマスターなどでサイトの情報を登録します。</p>
				<p>こちらはオプションとなることが多いので、代理登録をするか、クライアント様自身に登録してもらうかをあらかじめ決めておきます。</p>
			</section>
			<section class="numbering">
				<h3>ウェブサイトマーケティング支援</h3>
				<p>ウェブサイト制作および公開後における一連の流れをご紹介させていただきました。ウェブサイトの規模や制作メンバーの人数によっても変わってきますが、基本的な制作フローは以上になります。</p>
				<p>最近では、一部もしくは全体の作業をクライアント様自身で制作から公開までおこなう企業様も珍しくなくなってきました。しかし、こうした戦略的ウェブサイト制作の流れを知らないと、あとで思ったとおりのデザインにならないことがあります。また、作り手のクライアント様の趣味嗜好に傾倒しユーザに思うようにヒットしないウェブサイトができてしまいがちです。そのためにも当社のような専門の制作会社に委託し、ターゲット選定からしっかりと情報を共通認識化することで、ウェブサイトの目的自体が変わった時でも対応できるようになります。</p>
				<p>当社では、目的に合わせて現状の課題を抽出し、ユーザの興味をひく企画立案・インタビュー・写真撮影・コンテンツ開発・ウェブサイト制作・自ドメイン取得代行・レンタルサーバ公開・公開後の徹底した分析・検証に基づいた目的達成・課題の解決策を目指し、中長期を見越した継続的な戦略により、良質なウェブサイト運用ができる体制構築をサポートいたします。</p>
			</section>
		</section>
    <div class="btn_service df jc-c">
      <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
    </div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>