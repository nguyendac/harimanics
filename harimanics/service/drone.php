<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service salespromotion';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="ドローン空撮">DRONE</h1>
		<section class="subject">
			<p>効果的な ビジュアルインパクトを 発揮します。</p>
			<p>印刷物、Web、PRに。 新しい景色をとりいれませんか？</p>
		</section>
		<section class="subject">
			<h2 class="diamond">動画・静止画</h2>
			<p>ドローンを使用したお望みの動画・静止画を撮影します。</p>
		</section>
		<section class="subject">
			<h2 class="diamond">保守点検・調査</h2>
			<p>これまで足場を組んだり高所作業車で行っていた、大型施設の保守点検や建設現場の進捗状況の確認などが短時間でより安価で行えます。</p>
		</section>
		<section class="subject">
			<h2 class="diamond">より安価に</h2>
			<p>静止画 4万5千円〜 動画 6万円〜  ※天候によるキャンセル料はいただきません。</p>
		</section>
		<section class="subject">
			<h2 class="diamond">国土交通省大阪航空局認可</h2>
			<p>全国包括申請認可を国土交通省よりいただいております。</p>
			<p>お急ぎの空撮依頼や撮影箇所の急な追加などにも対応できます。</p>
			<dl class="kyoka">
				<dt>許可・承認を得た項目</dt>
				<dd>
					<ul>
						<li>人口集中地区での飛行</li>
						<li>建物等から30m以内に近づいての飛行</li>
						<li>夜間の飛行</li>
						<li>目視外飛行</li>
					</ul>
				</dd>
			</dl>
			<p>詳細につきましてはお気軽にお問い合わせくださいませ。</p>
			<p><small>※飛行エリアによっては敷地所有者、建造物所有者への承認が必要です。</small></p>
		</section>
		<section class="subject">
			<h2 class="diamond">経験豊富なオペレーター</h2>
			<p>経験豊富なオペレーターが撮影現場の状況を瞬時に判断し、最善を尽くします。</p>
			<p>安心安全には十分配慮し撮影を行います。</p>
			<p>技能認定資格DJIスペシャリスト（2名）</p>
		</section>
		<section class="subject">
			<h2 class="diamond">編集</h2>
			<p>撮影後の編集もお任せください。</p>
			<p>お客様ご要望のファイル形式で納品します。</p>
		</section>
		<section class="subject">
			<h2 class="diamond">空撮以外の撮影・編集</h2>
			<p>空撮だけでなく各種撮影もご相談承ります。</p>
		</section>
		<section class="subject">
			<h2 class="diamond">企画制作空撮番組</h2>
			<p>DISCOVER HYOGO BANBANネットワークス　連日放送中</p>
		</section>
	</article>
	<div class="subject works_links">
		<a href="http://空撮館.com/" target="_blank" class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once"><img src="/img/btn_kusatukan.png" alt=""></a>
		<a href="http://空撮館.com/discover_hyogo/" target="_blank" class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once"><img src="/img/btn_discover.png" alt=""></a>
	</div>
  <div class="btn_service df jc-c">
    <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
  </div>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>