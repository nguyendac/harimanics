<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service';

require_once '../php/.header.php';//ヘッダー読み込み
include_once 'img/symbol-defs.svg';
?>

<main role="main">
	<article>
		<h1 id="sales" data-notation="セールスプロモーション部">SALES PROMOTION</h1>
		<div class="sales_container subject df fd-r fw-w jc-sb">
			<a class="sa" href="website.php">
				<svg class="si">
					<use xlink:href="#si_website" />
				</svg>
				<span>WEBサイト制作</span>
			</a>
			<a class="sa" href="webmarketing.php">
				<svg class="si">
					<use xlink:href="#si_webmarketing" />
				</svg>
				<span>WEBマーケティング</span>
			</a>
			<a class="sa" href="websystem.php">
				<svg class="si">
					<use xlink:href="#si_websystem" />
				</svg>
				<span>WEBシステム開発</span>
			</a>
			<a class="sa" href="illustration.php">
				<svg class="si">
					<use xlink:href="#si_illustration" />
				</svg>
				<span>イラストレーション制作</span>
			</a>
			<a class="sa" href="drone.php">
				<svg class="si">
					<use xlink:href="#si_drone" />
				</svg>
				<span>ドローン空撮</span>
			</a>
			<a class="sa" href="movie.php">
				<svg class="si">
					<use xlink:href="#si_video" />
				</svg>
				<span>動画制作</span>
			</a>
			<a class="sa" href="print.php">
				<svg class="si">
					<use xlink:href="#si_print" />
				</svg>
				<span>印刷・出力・加工</span>
			</a>
		</div>
	</article>
	<article>
		<h1 id="engineering" data-notation="エンジニアリングソリューション部">ENGINEERING SOLUTION</h1>
		<div class="enginnering_container subject df fd-r fw-w jc-sb">
			<a class="sa" href="copy_and_scan.php">
				<svg class="si">
					<use xlink:href="#si_scan" />
				</svg>
				<span>スキャニング・大量コピー</span>
			</a>
			<a class="sa" href="material.php">
				<svg class="si">
					<use xlink:href="#si_material" />
				</svg>
				<span>資料作成支援</span>
			</a>
			<a class="sa" href="design_and_drafting.php">
				<svg class="si">
					<use xlink:href="#si_cad" />
				</svg>
				<span>設計・製図</span>
			</a>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>