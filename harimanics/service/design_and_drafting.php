<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service enginnering';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="設計・製図">DESIGN / DRAFTING</h1>
		<section class="subject">
			<h2 class="diamond">製図業務のサポート</h2>
			<p>AutoCAD・Creoによる2D・3D図面作成及び各種データ処理・資料作成、A0までの大型図面出力、2Dデータの3D化、3Dプリンタ入稿データ作成、各種調査支援</p>
		</section>
		<section class="subject">
			<h2 class="diamond">多岐にわたるCADの活用</h2>
			<p>A0までの大型図面出力もお任せください。</p>
		</section>
		<section class="subject">
			<h2 class="diamond">2Dから3Dへ</h2>
			<p>3Dプリンタの普及と共に3D-CADの需要も高まってきております。</p>
			<p>3Dプリンタ入稿データの作成などもご相談下さい。</p>
		</section>
    <div class="btn_service df jc-c">
      <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
    </div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>