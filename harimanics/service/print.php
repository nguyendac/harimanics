<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service salespromotion';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="印刷・出力・加工">PRINTING</h1>
		<section class="subject">
			<p>ハリマニックス株式会社では、少部数印刷に対応できる設備を自社内に備えることで、より具体的なイメージの共有や、お客様の急なご要望にお応えする体制を築いております。</p>
			<p>お気軽にご相談ください。</p>
		</section>
		<section class="subject">
			<h2 class="circle">少部数からお受けします。</h2>
			<p>他社では対応できない少部数の出力も快くお受けいたします。</p>
		</section>
		<section class="subject">
			<h2 class="circle">超短納期にも快く対応いたします。</h2>
			<p>タイトな納期の出力やコピーなど、ご相談ください。</p>
			<p>納期厳守で満足いただける仕上がりでお届けいたします。</p>
		</section>
		<section class="subject">
			<h2 class="circle">手作業もお受けいたします。</h2>
			<p>機械を使った加工になるとどうしても限りができてしまいます。</p>
			<p>お客様の思いを形にするため、手作業による加工も行います。</p>
		</section>
		<hr class="subject">
		<section class="subject">
			<h2 class="diamond">オンデマンド印刷</h2>
			<p>カラー・モノクロコピー、名刺、各種カード、はがき、チラシ、ポスター、フライヤー、リーフレット、パンフレット、冊子など</p>
		</section>
		<section class="subject">
			<h2 class="diamond">大型出力</h2>
			<p>A3サイズ〜A0サイズまで対応致します。</p>
		</section>
		<section class="subject">
			<h2 class="diamond">加工</h2>
			<p>各種製本（無線綴じ・中綴じ）、折加工（二つ折り・三つ折り・ＤＭ折り・観音折り）、ミシン・すじ入れ加工、天のり加工、穴あけ加工</p>
		</section>
		<section class="subject">
			<h2 class="diamond">ラミネート加工</h2>
			<p>名刺サイズ〜Ｂ０サイズ、</p>
		</section>
		<section class="subject">
			<h2 class="diamond">手作業</h2>
			<p>型押し加工・角丸加工など</p>
		</section>
	</article>
	<article>
		<h1 data-notation="ラミネート加工">LAMINATE</h1>
		<section class="subject">
			<p>カードサイズからB0大判ポスターまで、豊富な種類のラミネート加工サービスをご提供いたします。</p>
			<p>用途に合わせて、フィルムの厚みを変えたり特殊加工も可能です。一万枚を超える大量ラミネート加工もお任せ下さい。</p>
			<p>市販のラミネーターでは実現できないプロ品質の仕上がりを、お手軽に安価でお求めいただけます！</p>
		</section>
	</article>
	<div class="subject works_links">
		<a href="https://www.laminate-online.net/" target="_blank" class="animated" data-scroll="toggle(.fadeIn, .invisible) addHeight once"><img src="/img/btn_laminate.png" alt=""></a>
	</div>
  <div class="btn_service df jc-c">
    <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
  </div>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>