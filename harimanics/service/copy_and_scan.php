<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='サービス部門';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='service enginnering';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="スキャニング・大量コピー">SCANNING / COPY</h1>
		<section class="subject">
			<h2></h2>
			<p>紙媒体の書類を電子データ化(モノクロ、カラー共に最大A0ロング対応)、フィルムスキャン(マイクロ、ACカード、工業用X線フィルム、ネガ・ポジフィルム)、スキャニングデータ編集・整理</p>
			<p>仕上りデータ形式：PDF、TIF、JPEG、BMP等</p>
			<p>保管用CD/DVD/BD作成、暗号化書き込み、ラベル・ジャケット作成</p>
			<p>各種データのモノクロ、カラー共にB0サイズまでの出力、及びコピー、各種製本加工</p>
      <div class="btn_service df jc-c">
        <a class="btn btn_viewmore animated fadeIn" data-scroll="toggle(.fadeIn, .invisible) addHeight once" href="/service">GO BACK</a>
      </div>
		</section>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>