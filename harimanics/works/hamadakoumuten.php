<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>株式会社浜田工務店</h2>
					<a href="http://recruit-hamada.jp/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>WEBデザイン</li>
							<li>コーディング</li>
							<li>ドローン空撮・動画編集</li>
							<li>企画</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>公式採用サイト新規制作</div>
							</li>
							<li>
								<div>サイト名</div>
								<div>浜田工務店 公式採用サイト</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div>「求人サイト」と「採用サイト」の良いとこどりを備えた、ハイブリッドな特化サイトを目指す。</div>
							</li>
							<li>
								<div>案件背景</div>
								<div>現在採用ツールとして他社WEBサービスやその他媒体が主になっており、採用単価の削減と採用効率の向上が必須となっている。</div>
							</li>
							<li>
								<div>サイトの目的</div>
								<div>自社で採用サイトを持ち安定運用させることで、他社WEBサービスに使っている広告費用を減らし、1人あたりの採用単価を減らす。</div>
							</li>
							<li>
								<div>サイト制作必須要件</div>
								<div>サポートOS：Windows7・8・10以降、Mac OS X 以降、Android、iOS<br>
サポートブラウザ：Internet Explorer 11以降、FireFox最新版、Chrome最新版<br>
言語：HTML5、PHP、JavaScript<br>DB：MYSQL</div>
							</li>
							<li>
								<div>コンバージョン（数値目標）</div>
								<div>設定なし</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/hamada_01.png" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>