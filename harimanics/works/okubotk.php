<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>株式会社大窪鉄工所</h2>
					<a href="http://www.okubotk.co.jp/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>WEBデザイン</li>
							<li>コーディング</li>
							<li>ドローン空撮・動画編集</li>
							<li>撮影</li>
							<li>企画</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>Webサイトリニューアル</div>
							</li>
							<li>
								<div>サイト名</div>
								<div>株式会社大窪鉄工所</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div>会社の勢いを反映させた、現在のユーザー向けに照準を合わせたデザイン・コンテンツ作り！</div>
							</li>
							<li>
								<div>案件背景</div>
								<div>既にWebサイトは公開されている。現在更新が出来なくなっており、また全体的に古くなった。スマホ非対応。</div>
							</li>
							<li>
								<div>サイトの目的</div>
								<div>企業のブランディングツールとしての醸成。リクルート・営業、両面でのツールとして活用できるようにする。</div>
							</li>
							<li>
								<div>サイト制作必須要件</div>
								<div>IE11以上／Chrome ・ Firefoxは最新版対応<br>スマホ対応（レスポンシブ）</div>
							</li>
							<li>
								<div>コンバージョン（数値目標）</div>
								<div>設定なし</div>
							</li>
							<li>
								<div>ホームページ掲載用 企業PR動画</div>
								<div>企業イメージの向上。</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/okubotk_01.png" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>