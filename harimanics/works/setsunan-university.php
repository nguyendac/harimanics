<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>摂南大学</h2>
					<a href="http://www.setsunan.ac.jp/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>ドローン空撮・動画編集</li>
							<li>撮影</li>
							<li>編集</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>サイト名</div>
								<div>摂南大学</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div><p>新しい視点、新しい摂南大学</p><p>WEBサイトリニューアルにあわせメインビジュアル動画を担当しました。<br>ドローンを使い摂南大学の校舎、学内の様子を撮影しました。映像の浮遊感を統一するため、地上撮影もドローンを手持ちし撮影しました。</p><p>また付近の景観、地域性をアピールするための工夫や、コンセプトにある「新しい視点」を表現するため、夕暮れから夜間の撮影も行いました。<br>マジックアワーでの撮影は現場にいたスタッフ一同感動しっぱなしでした。</p><p>学校法人 常翔学園理事長賞受賞</p><p><small>＊撮影にあたり、寝屋川警察署、淀川河川事務所のご協力をいただいております。</small></p></div>
							</li>
							<li>
								<div>ホームページ掲載用 構内外観紹介</div>
								<div>入学希望者向けに、構内外観と立地環境を中心に紹介。</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/setsunan_01.png" alt="">
					<img src="img/setsunan_02.png" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>