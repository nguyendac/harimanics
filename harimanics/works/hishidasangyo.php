<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>菱田産業株式会社</h2>
					<a href="http://hishidasangyo.co.jp/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>WEBデザイン</li>
							<li>コーディング</li>
							<li>ドローン空撮・動画編集</li>
							<li>企画</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>Webサイト新規制作</div>
							</li>
							<li>
								<div>サイト名</div>
								<div>菱田産業株式会社</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div>勝てるところで勝負する！<br>菱田産業ならではの魅力を深く掘り下げる！</div>
							</li>
							<li>
								<div>案件背景</div>
								<div>現在は一定の派遣先を確保できており、お客様からの引き合いも少なくはない。<br>ただし、今後大きく事業拡大していくには、会社の認知度をより高める事と、お客様へ事業の更なる信頼性を与えることが必須と考えられている。<br>また他派遣会社と比べても強みとなる部分があるにもかかわらず（例：無期雇用等）、そのことが上手く発信できていない。<br>逆に一般的な人材派遣会社ほどの求人案件はなく、現状ユーザー側に多くの案件を紹介することは難しい。<br>将来的には地域限定（高砂）→東播→兵庫県→関西圏という具合に事業拡大・エリア拡大も考えられている。<br>現在、菱田産業としてのロゴやキャッチコピー、会社スローガンなどはなし。人材派遣サービスも特段別名称で行う予定はなし。</div>
							</li>
							<li>
								<div>サイトの目的</div>
								<div>①会社の認知促進を図る<br>②求人担当者への信頼感UP（→魅力・強み・安心）<br>③求職者への求人情報露出UP（→人材募集数の拡大・速度のUP）</div>
							</li>
							<li>
								<div>サイト制作必須要件</div>
								<div>IE11以上、他ブラウザ最新バージョン及び一世代前まで。<br>スマホ・タブレット対応（レスポンシブ）<br>iPhone、Andoroid両対応</div>
							</li>
							<li>
								<div>コンバージョン（数値目標）</div>
								<div>リリース後、半年くらいで月間問い合わせ、エントリー、月間セッション、月間インプレッション設定あり<br>（※但し自然SEOに限る）</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/hishidasangyo_01.png" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>