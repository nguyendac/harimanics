<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>ロート製薬株式会社</h2>
					<a href="https://jp.rohto.com/shinryoku/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>WEBデザイン</li>
							<li>コーディング</li>
							<li>ドローン空撮・動画編集</li>
							<li>企画</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>ブランドサイト新規制作</div>
								<div>（サイトデザイン、コーディング、クライアントCMSサーバアップ）</div>
							</li>
							<li>
								<div>ブランド名</div>
								<div>ロート新緑</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div></div>
							</li>
							<li>
								<div>案件背景</div>
								<div>
									<dl>
										<div>
											<dt>目的</dt>
											<dd>「目やに」へフォーカスし、目の炎症との関係性をクリアにすることで、何のために点す目薬なのかということを強調した製品紹介としたい。</dd>
										</div>
										<div>
											<dt>ストーリー</dt>
											<dd>多くの方が共感する問題提起から始まり、その原因とメカニズムを明らかにしたうえで、製品の処方・特徴・PR部分につなげる。</dd>
										</div>
										<div>
											<dt>トーン＆マナー</dt>
											<dd>新パッケージを参考に、ターゲットである60歳前後の方に受け入れられやすい色使い・文字サイズ・レイアウトとする。生薬由来ということが伝わるように、「さわやかさ」・「気持ちよさ」を感じるような世界観を演出する。</dd>
										</div>
										<div>
											<dt>注意点</dt>
											<dd>「目やに」には注釈「目のかすみ（目やにが多い時など）」を入れる。<br>「目やにに効く」とまでは断言せず、「気になる目やにに」くらいのマイルドな表現を使う。</dd>
										</div>
									</dl>
								</div>
							</li>
							<li>
								<div>サイト制作必須要件</div>
								<div>他社CMSサーバーへアップ納品</div>
							</li>
							<li>
								<div>コンバージョン（数値目標）</div>
								<div>設定なし</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/rohto_01.png" alt="">
					<img src="img/rohto_02.png" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>