<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="works_container subject">
			<?php require_once 'works_menu.php' ?>
			<div class=" df fw-w jc-sb">
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/kounotorakku.jpg?v=2" alt="">
					<figcaption>
						<h2>河野トラック株式会社</h2>
					</figcaption>
					<a href="/works/kounotorakku.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/takasago.jpg?v=2" alt="">
					<figcaption>
						<h2> 高砂市観光交流ビューロー</h2>
					</figcaption>
					<a href="/works/takasago-tavb.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/setsunan.jpg?v=2" alt="">
					<figcaption>
						<h2>摂南大学</h2>
					</figcaption>
					<a href="/works/setsunan-university.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/epinygel.jpg?v=2" alt="">
					<figcaption>
						<h2>Epiny Gel</h2>
					</figcaption>
					<a href="/works/epinygel.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/okubotekkou.jpg?v=2" alt="">
					<figcaption>
						<h2>株式会社大窪鐵工所</h2>
					</figcaption>
					<a href="/works/okubotk.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/hiroshimakokusai.jpg?v=2" alt="">
					<figcaption>
						<h2>広島国際大学</h2>
					</figcaption>
					<a href="/works/hirokoku-u.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/daieirasen.jpg?v=2" alt="">
					<figcaption>
						<h2>株式会社大栄螺旋工業</h2>
					</figcaption>
					<a href="/works/daieirasen.php"></a>
				</figure>
				<!-- <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/yakitatei.jpg?v=2" alt="">
					<figcaption>
						<h2>yakitatei</h2>
					</figcaption>
					<a href="/works/"></a>
				</figure> -->
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/rohto.jpg" alt="">
					<figcaption>
						<h2>ロート製薬株式会社</h2>
					</figcaption>
					<a href="/works/rohto_shinryoku.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/nakachu.jpg" alt="">
					<figcaption>
						<h2>株式会社ナカチュー</h2>
					</figcaption>
					<a href="/works/nakachu.php"></a>
				</figure>
        <figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/hamada.jpg" alt="">
					<figcaption>
						<h2>株式会社浜田工務店</h2>
					</figcaption>
					<a href="/works/hamadakoumuten.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/hishidasangyo.jpg" alt="">
					<figcaption>
						<h2>菱田産業株式会社</h2>
					</figcaption>
					<a href="/works/hishidasangyo.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/no_image.png" alt="">
					<figcaption>
						<h2>神戸薬科大学</h2>
					</figcaption>
					<a href="/works/kobepharma-u.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/no_image.png" alt="">
					<figcaption>
						<h2>オータニにしき<br>カントリークラブ</h2>
					</figcaption>
					<a href="/works/nishiki-cc.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/no_image.png" alt="">
					<figcaption>
						<h2>中谷建材株式会社</h2>
					</figcaption>
					<a href="/works/nakatanikenzai.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/no_image.png" alt="">
					<figcaption>
						<h2>コルサディマッキナ2016</h2>
					</figcaption>
					<a href="/works/corsa_di_macchina.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/no_image.png" alt="">
					<figcaption>
						<h2>大岡ゴルフクラブ</h2>
					</figcaption>
					<a href="/works/ookagolf.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/no_image.png" alt="">
					<figcaption>
						<h2>バーニーズスウィート</h2>
					</figcaption>
					<a href="/works/barneys_sweet.php"></a>
				</figure>
				<figure class="work animated" data-scroll="toggle(.fadeInUp, .invisible) addHeight once">
					<img src="/works/img/no_image.png" alt="">
					<figcaption>
						<h2>カワムラサイクル</h2>
					</figcaption>
					<a href="/works/kawamura-cycle.php"></a>
				</figure>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>