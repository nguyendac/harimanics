<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>株式会社カワムラサイクル</h2>
					<a href="http://www.kawamura-cycle.co.jp/kawamura_hp/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>WEBデザイン</li>
							<li>コーディング</li>
							<li>ドローン空撮・動画編集</li>
							<li>企画</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>Webサイトリニューアル</div>
							</li>
							<li>
								<div>サイト名</div>
								<div>株式会社カワムラサイクル</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div>B to C for C<br>世界に誇るKOBE JAPANブランド、カワムラサイクルの車いす</div>
							</li>
							<li>
								<div>案件背景</div>
								<div>これまでのカワムラサイクル様の企業イメージは「地味・ダサい」だった<br>そんな企業イメージを払拭したい<br>福祉用具業界全体では、ベンチャーや海外企業の参入が多くあり、安くてデザイン良い新製品が多くある<br>カワムラサイクルの車いすの値段は安くない<br>しかも特筆すべき機能や特徴もない<br>そこでーKOBE JAPAN－というブランドイメージと創業当時から安定した品質の良さ、耐久性の強さをアピールしたい</div>
							</li>
							<li>
								<div>サイトの目的</div>
								<div>更なる売り上げ増<br>-KOBE JAPAN-ブランドイメージの確立と定着<br>社員のモチベーションアップ<br>あくまで法人向けだが、個人消費者にも伝わる内容・見せ方にこだわる</div>
							</li>
							<li>
								<div>サイト制作必須要件</div>
								<div>スタイリッシュなデザイン<br>英文・中文サイト<br>動画コンテンツ<br>保守対応しやすい仕様</div>
							</li>
							<li>
								<div>コンバージョン（数値目標）</div>
								<div>月間PV数は1万を超えるB to C for Cを目指す</div>
							</li>
							<li>
								<div>展示会用プロモーション動画</div>
								<div>製品紹介と企業ブランドの向上</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/" alt="">
					<img src="img/" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>