<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>河野トラック株式会社</h2>
					<a href="http://www.kounotorakku.jp/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb kouno">
							<li>WEBデザイン</li>
							<li>コーディング</li>
							<li>ドローン空撮・動画編集</li>
							<li>撮影</li>
							<li>企画</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>Webサイト新規制作</div>
							</li>
							<li>
								<div>サイト名</div>
								<div>河野トラック</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div>クリーンでおしゃれなデザインとUI</div>
							</li>
							<li>
								<div>案件背景</div>
								<div>事業拡張のため、サイトをつくり信頼感、企業イメージを高めたい。</div>
							</li>
							<li>
								<div>サイト目的</div>
								<div>企業イメージの向上。<br>事業は好調だが、運送業界へのイメージが良くないため人材が不足している。<br>サイト閲覧者に、クリーンな印象をもってもらいたい。</div>
							</li>
							<li>
								<div>サイト制作必須要件</div>
								<div>若者向けのリクルートページ<br>所有特殊トラックの掲載（日本に数台しかないトラックを保有する）<br>おしゃれでクリーンな印象のデザインが良い<br>後でファーストビューの空撮動画をみせる可能性あり<br>問い合わせフォームは無しだが、電話番号など連絡先は掲載したい</div>
							</li>
							<li>
								<div>コンバージョン（数値目標）</div>
								<div>問い合わせ増</div>
							</li>
							<li>
								<div>ホームページ掲載用 企業PR動画</div>
								<div>企業イメージの向上と所有トラック＆トレーラーの紹介。</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/kounotorakku_01.jpg" alt="">
					<img src="img/kounotorakku_02.jpg" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>