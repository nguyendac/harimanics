<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>高砂観光ビューロー</h2>
					<a href="http://www.takasago-tavb.com/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>WEBデザイン</li>
							<li>コーディング</li>
							<li>ドローン空撮・動画編集</li>
							<li>企画</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>Webサイト新規制作</div>
							</li>
							<li>
								<div>サイト名</div>
								<div>高砂観光ビューロー</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div>わかりやすく綺麗に！<br>サイトを見た方が高砂に行ってみたいと思わせる！</div>
							</li>
							<li>
								<div>案件背景</div>
								<div>高砂市の観光、文化発信の中枢を担うポータルサイトを作成されたい。<br>他府県、海外からの観光客の窓口に。</div>
							</li>
							<li>
								<div>サイトの目的</div>
								<div>高砂市の観光、文化発信の中枢を担うサイト<br>他府県、海外からの観光の窓口に。<br>多言語化や高齢の方が見てもわかりやすい。<br>イベント情報等の告知ができる。<br>交通機関の丁寧な案内</div>
							</li>
							<li>
								<div>サイト制作必須要件</div>
								<div>イベント情報等の告知<br>交通機関の丁寧な案内<br>検索機能<br>チラシ等のわかりやすいダウンロード</div>
							</li>
							<li>
								<div>コンバージョン（数値目標）</div>
								<div>無し</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/takasago_01.png" alt="">
					<img src="img/takasago_02.png" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>