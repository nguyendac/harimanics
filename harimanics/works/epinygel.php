<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>株式会社バーニーズ・スイート</h2>
					<a href="http://www.epinygel.jp/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>WEBデザイン</li>
							<li>コーディング</li>
							<li>ドローン空撮・動画編集</li>
							<li>企画</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>Webサイトリニューアル</div>
							</li>
							<li>
								<div>サイト名</div>
								<div>エピニージェル（Epiny Gel）</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div>信頼性、安全性、使用する者の優越感、安心感をアピール<br>ペルソナはネイルサロンのスタッフやスクールの講師<br>※文面は女性を意識しすぎない。広い範囲でアピールしたい。</div>
							</li>
							<li>
								<div>案件背景</div>
								<div>株式会社バーニーズ・スイートが販売するオリジナルジェルネイル「EPNY GEL」の販促サイトをリニューアル。<br>「EPNY JEL」は、今まで楽天等を利用したネット販売とバーニーズ・スイートが展開しているネイルスクール等での販売に限られ、その普及は顧客層に限られていた。そこでより効果的な販促ツールを整備されるとの事。<br>今回のサイトリニューアルもその事業の一環である。</div>
							</li>
							<li>
								<div>サイトの目的</div>
								<div>より顧客を増やすための販促ツールとして。<br>BtoB</div>
							</li>
							<li>
								<div>サイト制作必須要件</div>
								<div>特徴・製品情報・アートギャラリー・活用法・Webカタログ(PDF)・よくある質問・お問い合わせ・トップページからのリンク集<br>お客様自身での更新出来るように<br>コーポレートサイトとしての役割も担う（但し会社概要のみ）</div>
							</li>
							<li>
								<div>コンバージョン（数値目標）</div>
								<div>問い合わせ増</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/epinygel_01.png" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>