<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='実績紹介';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='works';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article>
		<h1 data-notation="実績紹介" class="new_style"><a href="/works" data-notation="実績紹介">WORKS</a></h1>
		<div class="subject">
			<?php require_once 'works_menu.php' ?>
			<div class="pastwork_container df fd-r fd-r jc-sb">
				<div class="textarea">
					<h2>広島国際大学</h2>
					<a href="http://www.hirokoku-u.ac.jp/" target="_blank" class="site"></a>
					<div class="category">
						<ul class="df fd-r fd-r fw-w jc-sb">
							<li>ドローン空撮・動画編集</li>
							<li>撮影</li>
						</ul>
					</div>
					<div class="text">
						<ul class="this_case">
							<li>
								<div>サイト名</div>
								<div>広島国際大学</div>
							</li>
							<li>
								<div>プロジェクトテーマ（制作コンセプト）</div>
								<div><p>周囲が自然に囲まれ学業に専念できる環境のアピール</p><p>WEBサイトメインビジュアル用の映像をドローンを使用し撮影いたしました。</p><p>東広島キャンパスは秋の紅葉を、呉キャンパスは校舎上空から望む瀬戸内海など、それぞれの環境を丁寧にご紹介できるよう撮影いたしました。</p></div>
							</li>
							<li>
								<div>ホームページ掲載用 構内外観紹介</div>
								<div>入学希望者向けに、構内外観と立地環境を中心に紹介。</div>
							</li>
						</ul>
					</div>
				</div>
				<div class="imgarea">
					<img src="img/hirokoku_01.png" alt="">
					<img src="img/hirokoku_02.png" alt="">
				</div>
			</div>
		</div>
	</article>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>