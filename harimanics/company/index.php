<?php
$kw='実績,制作例,ハリマニックス,DTP,ウェブ,開発,コーディング,印刷,設計,営業,高砂,兵庫,大阪,関西';//metaのキーワード
$d='';//metaのdescription
$title='企業情報';//title要素のページ名部分
$og_title='';//OGPのタイトル
$og_type='article';//OGPのタイプ TOPは website 他は article
$og_url='';//OGPのurl ドメインは書いてあるのでファイル名くらい
$og_img='';//OGPのイメージ そのページがシェアされた時のキャッチ画像
$og_description='';//OGPのdescription
$canonical='<link rel="canonical" href="">';//link rel="canonical" の設定(無ければ空白)
$other01='';//その他、meta用(link要素より先にくるもの)
$other02='';//その他、/headの直前に入れる用
$bodyclass='company';

require_once '../php/.header.php';//ヘッダー読み込み
?>

<main role="main">
	<article class="greeting">
		<h1 data-notation="ご挨拶">TOP MESSAGE</h1>
		<div class="subject">
			<p>ＩＴ技術の急速な発展に伴い、情報伝達の在り方や捉え方、トレンドの中心が目まぐるしく変化する時代となりました。その様な中でハリマニックス株式会社は、いつまでもお客様のお役に立てる企業として有り続けたいと考え、日々挑戦を続けております。</p>
			<p>数年前から取り組んでいるドローンによる空撮事業をはじめ、現代のお客様の潜在ニーズを叶えることに重点を置いて経営に取り組んでおります。</p>
			<p>たった一つの言葉でも、人はそこに沢山のメッセージを感じ取れる。たった一つのメッセージから、目の前にいない人のことでも親身になって考えることが出来る。</p>
			<p>情報伝達という人間ならではのこの仕事を通して、仲間に対して、そしてお客様に対して情熱を注げるように、これからも成長を続けてまいります。</p>
			<p>今後共、末永くお引き立て下さいますよう宜しくお願い申し上げます。</p>
			<p class="sign">ハリマニックス株式会社<br>代表取締役 菱田 好美</p>
		</div>
	</article>
<!--
	<article class="vision">
		<h1 data-notation="VISION">理念・社訓</h1>
		<div class="subject df fd-r ">
				<div class="inner">
					<h2>企業理念</h2>
					<p>情報加工を通じて<br>真に有益なサービスを提供すると共に、<br>全従業員の物心両面の幸福を追求し、<br>社会の発展に貢献する</p>
				</div>
				<div class="inner">
					<h2>社訓</h2>
					<ul class="motto">
						<li><span>一、積極性</span>を身につけ何事にも勇気をもって立ち向おう</li>
						<li><span>一、協調性</span>を養い明るい職場を作ろう</li>
						<li><span>一、確実性</span>を身につけプロ意識をもち、誰にでも信頼される仕事をしよう</li>
						<li><span>一、創意性</span>を養い、現状に甘えず、常に工夫改善し、創造しよう</li>
					</ul>
				</div>
		</div>
	</article>
-->
	<article class="vision">
		<h1 data-notation="経営ビジョン・行動指針">VISION</h1>
		<div class="subject">
				<div class="inner">
					<h2>経営ビジョン</h2>
					<h3>つたわり、つながり、ひろがっていく。</h3>
					<p>報恩の連鎖で世の中は成長発展していきます。<br>その連鎖のお手伝いをすることで、ハリマニックスはお客様と共に成長してまいります。</p>
				</div>
				<div class="inner">
					<h2>行動指針</h2>
					<dl class="motto">
						<dt>実効性</dt>
						<dd>真にクライアントの役に立つ商品を提供することで喜び合える関係性を創り出そう。</dd>
						<dt>積極性</dt>
						<dd>何事にも積極的にかかわり勇気をもって立ち向かう力を身に着けよう。</dd>
						<dt>確実性</dt>
						<dd>確実な仕事力を身につけ、プロ意識を養って誰からも信頼される人物になろう。</dd>
						<dt>返報性</dt>
						<dd>常に与える気持ちで感謝の言葉が行き交う環境を創り出そう。</dd>
						<dt>協調性</dt>
						<dd>お互いに協力し合える明るい職場を創り出そう。</dd>
						<dt>創意性</dt>
						<dd>現状に甘えず、常に創意工夫や改善を行い、創造する力を養おう。</dd>
					</dl>
				</div>
				<div class="inner">
					<h2>人事ポリシー</h2>
					<dl class="motto">
						<dt>成果の協創</dt>
						<dd>市場の流れが加速する今、変化が起きてからでは間に合いません。<br>常に時代を先読みし、新たな価値をこの仲間たちと共に創造し続けよう。</dd>
					</dl>
				</div>
		</div>
	</article>
 	<article>
		<h1 data-notation="企業情報">COMPANY PROFILE</h1>
		<table class="subject com-info-tbl">
			<tr>
				<th>社名</th>
				<td>ハリマニックス株式会社<br>（旧 播磨コピー工業株式会社）</td>
			</tr>
			<tr>
				<th>設立</th>
				<td>1961年（昭和36年）9月</td>
			</tr>
			<tr>
				<th>代表者</th>
				<td>代表取締役 菱田 好美</td>
			</tr>
			<tr>
				<th>資本金</th>
				<td>1,250万円</td>
			</tr>
			<tr>
				<th>所在地</th>
				<td>本社 〒676-0022<br>兵庫県高砂市高砂町浜田町1-7-28</td>
			</tr>
			<tr>
				<th>ＴＥＬ</th>
				<td>079-443-5577（代表）</td>
			</tr>
			<tr>
				<th>ＦＡＸ</th>
				<td>079-443-1086（代表）</td>
			</tr>
			<tr>
				<th>ＵＲＬ</th>
				<td>http://www.harimanics.co.jp</td>
			</tr>
			<tr>
				<th>主要取引先</th>
				<td>三菱重工業株式会社／三菱日立パワーシステムズ株式会社／ＭＨＰＳエンジニアリング株式会社／富士ゼロックスサービスリンク株式会社／高砂市／加古川市／株式会社カネカ／播州信用金庫／サントリープロダクツ株式会社／AGC旭硝子／神戸工業試験場／小野福祉工場／伊東電機株式会社／株式会社カワムラサイクル／流通科学大学／摂南大学／広島国際大学／他</td>
			</tr>
			<tr>
				<th>取引銀行</th>
				<td>三井住友銀行、播州信用金庫</td>
			</tr>
			<tr>
				<th>従業員</th>
				<td>55名</td>
			</tr>
			<tr>
				<th>関連会社</th>
				<td>菱田産業株式会社</td>
			</tr>
			<tr>
				<th>事業内容</th>
				<td>・デザイン・プランニング事業<br>・WEB制作・IT関連事業<br>・デジタル・オンデマンド印刷事業<br>・資料作成・ファイリング事業<br>・CAD設計・製図事業<br>・アウトソーシング事業</td>
			</tr>
		</table>
	</article>
	<article class="history">
		<h1 data-notation="沿革">HISTORY</h1>
		<table class="subject history-tbl">
			<tr><th><span class="y">1961年</span><span class="m">9月</span><span class="era">昭和</span><span class="era-y">36年</span></th><td>播磨コピー工業株式会社設立</td></tr>
			<tr><th><span class="y">1966年</span><span class="m">9月</span><span class="era">昭和</span><span class="era-y">41年</span></th><td>事業発展に伴い移転</td></tr>
			<tr><th><span class="y">1973年</span><span class="m">4月</span><span class="era">昭和</span><span class="era-y">48年</span></th><td>菱田産業株式会社設立<br>業務分担の円滑化を図る</td></tr>
			<tr><th><span class="y">1979年</span><span class="m">5月</span><span class="era">昭和</span><span class="era-y">54年</span></th><td>事業拡張のため社屋を拡大</td></tr>
			<tr><th><span class="y">1989年</span><span class="m">7月</span><span class="era">平成</span><span class="era-y">元年</span></th><td>MACを導入</td></tr>
			<tr><th><span class="y">1995年</span><span class="m">1月</span><span class="era">平成</span><span class="era-y">7年</span></th><td>ファイルサーバの導入に伴いネットワーク環境を確立</td></tr>
			<tr><th><span class="y">1995年</span><span class="m">7月</span><span class="era">平成</span><span class="era-y">7年</span></th><td>株式会社ニックサービス（コンピュータ専門）設立</td></tr>
			<tr><th><span class="y">1998年</span><span class="m">1月</span><span class="era">平成</span><span class="era-y">10年</span></th><td>設計室設立CADシステムによる設計製図を始める</td></tr>
			<tr><th><span class="y">1998年</span><span class="m">11月</span><span class="era">平成</span><span class="era-y">10年</span></th><td>事業拡張及び福利厚生施設充実のため 隣地に社屋を設立</td></tr>
			<tr><th><span class="y">2001年</span><span class="m">5月</span><span class="era">平成</span><span class="era-y">13年</span></th><td>創始者菱田策三他界 社葬を執り行う</td></tr>
			<tr><th><span class="y">2001年</span><span class="m">10月</span><span class="era">平成</span><span class="era-y">13年</span></th><td>Xerox 社製 Color DocuTech60 導入</td></tr>
			<tr><th><span class="y">2002年</span><span class="m">10月</span><span class="era">平成</span><span class="era-y">14年</span></th><td>大日本スクリーン社製 TruePress544AD 導入</td></tr>
			<tr><th><span class="y">2005年</span><span class="m">3月</span><span class="era">平成</span><span class="era-y">17年</span></th><td>兵庫県加東市東条湖畔の別荘「菱のいゑ」を社員に開放<br>福利厚生保養所とする</td></tr>
			<tr><th><span class="y">2007年</span><span class="m">10月</span><span class="era">平成</span><span class="era-y">19年</span></th><td>大日本スクリーン社製 TruePress344 導入</td></tr>
			<tr><th><span class="y">2010年</span><span class="m">4月</span><span class="era">平成</span><span class="era-y">22年</span></th><td>Xerox 社製 Color1000 Press 導入</td></tr>
			<tr><th><span class="y">2011年</span><span class="m">1月</span><span class="era">平成</span><span class="era-y">23年</span></th><td>創立50周年播磨コピー工業(株)と(株)ニックサービスを統合<br>社名をハリマニックス株式会社とする</td></tr>
			<tr><th><span class="y">2012年</span><span class="m"></span><span class="era">平成</span><span class="era-y">24年</span></th><td>WEB事業部の立ち上げ<br>ECサイト「ラミネートオンライン」立ち上げ</td></tr>
			<tr><th><span class="y">2013年</span><span class="m">4月</span><span class="era">平成</span><span class="era-y">25年</span></th><td>菱田好美 専務取締役就任</td></tr>
			<tr><th><span class="y">2014年</span><span class="m">6月</span><span class="era">平成</span><span class="era-y">26年</span></th><td>大阪営業所を開設</td></tr>
			<tr><th><span class="y">2014年</span><span class="m">10月</span><span class="era">平成</span><span class="era-y">26年</span></th><td>カンプ専用機として Xerox 社製 DocuColor1450GA 導入</td></tr>
			<tr><th><span class="y">2015年</span><span class="m">10月</span><span class="era">平成</span><span class="era-y">27年</span></th><td>菱田好美 代表取締役に就任</td></tr>
			<tr><th><span class="y">2016年</span><span class="m"></span><span class="era">平成</span><span class="era-y">28年</span></th><td>WEB事業にて空撮サービスの開始</td></tr>
		</table>
	</article>
	<article id="anc-access" class="access">
		<h1 data-notation="アクセス">ACCESS</h1>
		<div class="subject fd-r jc-sa">
			<div class="place fg2">
				<h2>本社 <a href="" class="call_map btn btn_access call_access">ACCESS</a></h2>
				<p>〒676-0022&nbsp;&nbsp;&nbsp;&nbsp;兵庫県高砂市高砂町浜田町1丁目7-28</p>
				<p>TEL 079-443-5577&nbsp;&nbsp;&nbsp;&nbsp;FAX 079-443-1086</p>
			</div>
			<div class="place fg1">
				<h2>資料作成分室（第1分室）</h2>
				<p>〒676-0008&nbsp;&nbsp;&nbsp;&nbsp;兵庫県高砂市荒井町新浜2丁目8-25&nbsp;&nbsp;第1高砂菱興ビル5F</p>
				<p>TEL 079-443-7888&nbsp;&nbsp;&nbsp;&nbsp;FAX 079-443-7880</p>
			</div>
			<div class="place fg1">
				<h2>設計分室（第3分室）</h2>
				<p>〒676-0008&nbsp;&nbsp;&nbsp;&nbsp;兵庫県高砂市荒井町新浜2丁目8-30&nbsp;&nbsp;第3高砂菱興ビル1F</p>
				<p>TEL 079-443-3336</p>
			</div>
		</div>
	</article>
  <div id="paccess" class="popup">
    <div class="popup_main">
      <div id="content_paccess" class="popup_content_map">
        <div id="access" class="access">
  <p><img src="/img/logo.svg" alt=""></p>
	<div id="map" style="width: 100%; height: 320px; position: relative; overflow: hidden;"><div style="height: 100%; width: 100%; position: absolute; top: 0px; left: 0px; background-color: rgb(229, 227, 223);"><div class="gm-err-container"><div class="gm-err-content"><div class="gm-err-icon"><img src="https://maps.gstatic.com/mapfiles/api-3/images/icon_error.png" draggable="false" style="user-select: none;"></div><div class="gm-err-title">Oops! Something went wrong.</div><div class="gm-err-message">This page didn't load Google Maps correctly. See the JavaScript console for technical details.</div></div></div></div></div>
	<script>
		function initMap() {
			var uluru = {lat: 34.752842, lng: 134.800621};
			var map = new google.maps.Map(document.getElementById("map"), {
				zoom: 14, center: uluru, disableDefaultUI: true
			});
			var contentString = '<h2>ハリマニックス株式会社</h2><p>〒676-0022</p><p>兵庫県高砂市高砂町浜田町 1-7-28</p><p>Tel: 078-443-5577</p><a href="https://goo.gl/maps/1DKj3fTgfsJ2" target="_blank" class="gmapbtn">Google map</a>';
			var infowindow = new google.maps.InfoWindow({
				content: contentString
			});
			var imgPath = "/icon/icon_black.svg";
			var img = { url: imgPath, scaledSize: new google.maps.Size(36,36) };
			var marker = new google.maps.Marker({ position: uluru, map: map, icon: img });
			marker.addListener("click", function() {
				infowindow.open(map, marker);
			});
			var mapStyle = [
				{ "stylers": [ { "saturation": -100 } ] },
				{ "featureType": "poi.business", "elementType": "labels", "stylers": [ { "visibility": "off" } ] }
			];
			var mapType = new google.maps.StyledMapType(mapStyle);
			map.mapTypes.set("GrayScaleMap", mapType);
			map.setMapTypeId("GrayScaleMap");
		}
	</script>
	<script async="" defer="" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBqVnXGgDZa9SCLsj3AVqccBS6QMiW09oc&amp;callback=initMap"></script>
</div><!-- /class="access" -->        <div class="place fg2">
  				<h2>本社</h2>
  				<p>〒676-0022</p>
          <p>兵庫県高砂市高砂町浜田町1丁目7-28</p>
  				<p>TEL 079-443-5577<br>FAX 079-443-1086</p>
  			</div>
      </div>
      <a class="close" id="close" href="#">close</a>
    </div>
  </div>
</main>
<?php
	$harimap=''
?>
<?php require_once '../php/.footer.php';//フッター読み込み ?>