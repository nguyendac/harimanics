window.addEventListener('DOMContentLoaded',function(){
  new Slide();
})
var Slide = (function(){
  function Slide(){
    var s = this;
    this.target = 'mv';
    this.step  = 0.0005;
    this.scale = 1;
    this.currentSlide = 0;
    this.opacity = 0;
    this.timer;
    this.timeout;
    this.img;
    this.eles = document.getElementById(this.target).querySelectorAll('figure img');
    this.func_transition = function(){
      s.img = s.eles[s.currentSlide];
      closest(s.img,'figure').style.opacity = 1;
      closest(s.img,'figure').classList.add('active');
      s.scale+=s.step;
      if(s.scale >= 1.15){
        closest(s.img,'figure').style.opacity = 0;
        if(s.scale >= 1.18) {
          closest(s.img,'figure').classList.remove('active');
          s.currentSlide+=1;
          s.scale = 1;
          if(s.currentSlide > s.eles.length-1) {
            s.currentSlide = 0;
          }
        }
      }
      s.timer = window.requestAnimFrame(s.func_transition);
    }
    this.sizeWindow = function(){
      Array.prototype.forEach.call(s.eles,function(el,i){
        closest(el,'figure').style.opacity = s.opacity;
        closest(el,'figure').classList.remove('active');
      });
    }
    window.addEventListener('load',function(){
      s.sizeWindow();
    })
    // this.sizeWindow();
    this.func_transition();
  }
  return Slide;
})()